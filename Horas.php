<?php

namespace App\SKKLibrary;

use App\History;
use App\Notification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class Horas
{
    public static function send($users_id, $hp, $message, $referenceId)
    {
        $recipients = array();

        if (Utility::contains(',', $hp)) {
            $hp = explode(',', $hp);
            foreach ($hp as $hhp) {
                $front = substr($hhp, 0, 1);
                if ($front === "0") {
                    $tmp = config('variable.horas_cc') . substr($hhp, 1, strlen($hhp));
                    array_push($recipients, $tmp);
                } else {
                    array_push($recipients, $hhp);
                }
            }
        } elseif (is_array($hp)) {
            foreach ($hp as $hhp) {
                $front = substr($hhp, 0, 1);
                if ($front === "0") {
                    $tmp = config('variable.horas_cc') . substr($hhp, 1, strlen($hhp));
                    array_push($recipients, $tmp);
                } else {
                    array_push($recipients, $hhp);
                }
            }
        } else {
            $front = substr($hp, 0, 1);
            if ($front === "0") {
                $tmp = config('variable.horas_cc') . substr($hp, 1, strlen($hp));
                array_push($recipients, $tmp);
            } else {
                array_push($recipients, $hp);
            }
        }


        // save recipient to redis
        if (count($recipients) > 0) {
            Redis::sadd('notification:recipients', $referenceId);
            foreach ($recipients as $item) {
                $tmp = ['hp' => $item, 'nama' => null, 'type' => 'horas'];
                Redis::sadd('notification:recipients:' . $referenceId, json_encode($tmp));
            }
        }

        foreach ($recipients as $recipient) {
            Utility::addRecipient($recipient, null, 'horas');
        }

        $url = config('variable.horas_url') . "/v1/send";
        $body = [
            'message' => $message,
            'recipients' => $recipients,
            'number' => config('variable.horas_number'),
        ];
        $body = json_encode($body);

        $headers = array(
            'Content-Type: application/json',
        );

        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POST, true);
        curl_setopt($curlHandle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $body);
        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        curl_close($curlHandle);

        if ($err) {
            Handler::handleError(__METHOD__, $err);
        }

        $data = array(
            'users_id' => $users_id,
            'commands' => 'horas',
            'hp' => is_array($recipients) ? implode(',', $recipients) : $recipients,
            'reference_id' => $referenceId,
            'data' => is_array($curl) ? json_encode($curl) : $curl,
            'type' => '',
            'status' => 'sending',
        );
        $history = History::create($data);

        $notif = new Notification();
        $notif->users_id = $users_id;
        $notif->type = 'horas';
        $notif->hp = is_array($recipients) ? implode(',', $recipients) : $recipients;
        $notif->body = $message;
        $notif->reference_id = $referenceId;
        $notif->history_id = $history->id;
        $notif->save();

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);

        Log::debug(__METHOD__, $curl);
        return $curl;
    }

}
