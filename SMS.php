<?php

namespace App\SKKLibrary;

use App\History;
use App\Notification;
use App\User;
use Illuminate\Support\Facades\Log;

class SMS
{
    /**
     * @return mixed
     */
    public static function getBalance()
    {
        $result = array();
        $zenziva = SMS::getBalanceZenziva();
        $zenzivaMasking = SMS::getBalanceZenzivaMasking();
        $nexmo = SMS::getBalanceNexmo();

        $result['zenziva'] = $zenziva;
        $result['zenziva_masking'] = $zenzivaMasking;
        $result['nexmo'] = $nexmo;

        return json_decode(json_encode($result));
    }

    /**
     * @return mixed
     */
    public static function getBalanceNexmo()
    {
        $body = [
            'api_key' => config('variable.nexmo_userkey'),
            'api_secret' => config('variable.nexmo_passkey'),
        ];

        $body = http_build_query($body);
        $url = config('variable.nexmo_url') . '/account/get-balance?' . $body;

        $curlHandle = curl_init();
        curl_setopt_array($curlHandle, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
            ),
        ));

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);

        curl_close($curlHandle);

        if ($err) {
            Handler::handleError(__METHOD__, $err);
        }

        $dataResult = array(
            'value' => $results->value,
            'description' => 'EUR',
            'result' => $results,
        );

        return json_decode(json_encode($dataResult));
    }

    /**
     * @return mixed
     */
    public static function getBalanceZenziva()
    {
        $body = [
            'userkey' => config('variable.zenziva_userkey'),
            'passkey' => config('variable.zenziva_passkey'),
        ];

        $body = http_build_query($body);
        $url = config('variable.zenziva_url') . '/balance/?' . $body;
        $curlHandle = curl_init();
        curl_setopt_array($curlHandle, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
            ),
        ));

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);

        curl_close($curlHandle);

        if ($err) {
            Handler::handleError(__METHOD__, $err);
        }

        $dataResult = array(
            'value' => $results->credit,
            'description' => 'credit',
            'result' => $results,
        );

        return json_decode(json_encode($dataResult));
    }

    /**
     * @return mixed
     */
    public static function getBalanceZenzivaMasking()
    {
        $body = [
            'userkey' => config('variable.zenziva_masking_userkey'),
            'passkey' => config('variable.zenziva_masking_passkey'),
        ];

        $body = http_build_query($body);
        $url = config('variable.zenziva_masking_url') . '/getbalance.php?' . $body;
        $curlHandle = curl_init();

        curl_setopt_array($curlHandle, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
            ),
        ));

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);

        curl_close($curlHandle);

        if ($err) {
            Handler::handleError(__METHOD__, $err);
        }

        $dataResult = array(
            'value' => $results->Credit,
            'description' => 'credit',
            'result' => $results,
        );

        return json_decode(json_encode($dataResult));
    }

    /**
     * @param $users_id
     */
    public static function limitHandling($users_id)
    {
        if (Utility::getSetting('sms_zenziva_masking')) {
            $balance = SMS::getBalanceZenzivaMasking();
            $limit = Utility::getSetting('limit_sms_zenziva_masking');

            if ($balance->value < $limit) {
                SMS::sendNotification($users_id, 'zenziva-masking', $balance);
            }
        }

        if (Utility::getSetting('sms_zenziva')) {
            $balance = SMS::getBalanceZenziva();
            $limit = Utility::getSetting('limit_sms_zenziva');

            if ($balance->value < $limit) {
                SMS::sendNotification($users_id, 'zenziva', $balance);
            }
        }

        if (Utility::getSetting('sms_nexmo')) {
            $balance = SMS::getBalanceNexmo();
            $limit = Utility::getSetting('limit_sms_nexmo');

            if ($balance->value < $limit) {
                SMS::sendNotification($users_id, 'nexmo', $balance);
            }
        }
    }

    public static function sendNotification($users_id, $type, $balance)
    {
        $exist = History::where('commands', 'notif-' . $type)->where('created_at', 'like', date('Y-m-d') . '%')->first();
        if (!$exist) {
            $referenceId = Utility::generateRefId('NS', rand(111, 999));
            $chatIds = Utility::getSetting('balance_number_notify');
            $message = "Jumlah SMS *" . strtoupper($type) . "* tersisa `" . $balance->value . "` " . $balance->description;

            if (is_array($chatIds)) {
                foreach ($chatIds as $chatId) {
                    Telegram::send($users_id, $chatId, $message, null, null, $referenceId);
                }
            } else {
                Telegram::send($users_id, $chatIds, $message, null, null, $referenceId);
            }

            $history = new History();
            $history->users_id = $users_id;
            $history->commands = 'notif-' . $type;
            $history->save();
        }

    }

    /**
     * @param User $user
     * @param $hp
     * @param $message
     * @param string $target
     * @param null $referenceId
     * @param int $timeout
     * @return array
     */
    public static function send(User $user, $hp, $message, $target = 'mycoop', $referenceId = null, $timeout = 60)
    {
        $result = array();
        $recipients = ($hp != null) ? explode(',', str_replace(' ', '', $hp)) : null;
        if ($recipients && count($recipients) > 0) {
            foreach ($recipients as $recipient) {
                $send = false;

                if ($target === 'any' && $user->role === 'administrator') {
                    $send = true;
                }

                if (Utility::isAllowedNumber($recipient) && Utility::checkRecipientExist($user, $recipient, $target, false)) {
                    $send = true;
                }

                if ($send) {
                    Utility::addRecipient($recipient, Utility::getRecipentNameTarget($recipient, $target));

                    $historyId = 0;
                    if (Utility::getSetting('sms_zenziva')) {
                        $historyId = SMS::sendSMSZenziva($user->id, $recipient, $message, $timeout);
                    } elseif (Utility::getSetting('sms_zenziva_masking')) {
                        $historyId = SMS::sendSMSZenzivaMasking($user->id, $recipient, $message, $timeout);
                    } elseif (Utility::getSetting('sms_nexmo')) {
                        $historyId = SMS::sendSMSNexmo($user->id, $recipient, $message, $timeout);
                    } else {
                        Handler::handleMaintenance();
                    }

                    if ($historyId) {
                        History::find($historyId)->update(['reference_id' => $referenceId]);

                        $notif = new Notification();
                        $notif->users_id = $user->id;
                        $notif->type = 'sms';
                        $notif->hp = $recipient;
                        $notif->body = $message;
                        $notif->reference_id = $referenceId;
                        $notif->history_id = $historyId;
                        $notif->save();

                        // save to redis
                        SKKRedis::getData(new Notification(), [$notif->id]);
                        SKKRedis::getData(new History(), [$historyId]);
                    } else {
                        $data = array(
                            'hp' => $recipient,
                            'reference_id' => $referenceId,
                            'body' => $message,
                        );
                        Utility::simpleHistory('sms', 'send', 'gagal', null, $data);
                        $result['error'][] = ['hp' => $recipient, 'error' => true];
                    }
                } else {
                    $data = array(
                        'hp' => $recipient,
                        'reference_id' => $referenceId,
                        'body' => $message,
                    );
                    Utility::simpleHistory('sms', 'send', 'gagal', null, $data);
                    $result['error'][] = ['hp' => $recipient, 'error' => true];
                }
            }

            SMS::limitHandling($user->id);

            Log::debug(__METHOD__, $result);
            return $result;
        } else {
            Handler::handleWrongNumber();
            return null;
        }
    }

    /**
     * @param $users_id
     * @param $hp
     * @param $message
     * @param null $from
     * @param int $timeout
     * @return mixed
     */
    public static function sendSMSNexmo($users_id, $hp, $message, $from = null, $timeout = 60)
    {
        $hp = Utility::cleanNumber($hp); // menghilangkan simbol
        $front = substr($hp, 0, 1);
        if ($front === "0") {
            $hp = "62" . substr($hp, 1, strlen($hp));
        }

        $body = [
            'api_key' => config('variable.nexmo_userkey'),
            'api_secret' => config('variable.nexmo_passkey'),
            'to' => $hp,
            'text' => $message,
            'from' => ($from) ? $from : "NEXMO",
        ];

        $body = http_build_query($body);
        $url = config('variable.nexmo_url') . "/sms/json?$body";

        $headers[] = 'content-type: application/x-www-form-urlencoded';
        $curlHandle = curl_init($url);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $timeout);

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl, true);

        curl_close($curlHandle);

        if ($err) {
            Log::error(__METHOD__, ['err' => $err]);
            return null;
        } else {
            $data = array(
                'users_id' => $users_id,
                'commands' => 'sms',
                'transaction_id' => $results['messages'][0]['message-id'],
                'hp' => $hp,
                'body' => $message,
                'data' => json_encode($results),
                'type' => 'nexmo',
                'status' => 'sending',
            );
            $history = History::create($data);
            return $history->id;
        }
    }

    /**
     * @param $users_id
     * @param $hp
     * @param $message
     * @param int $timeout
     * @return mixed
     */
    public static function sendSMSZenziva($users_id, $hp, $message, $timeout = 60)
    {
        $url = config('variable.zenziva_url') . '/sendsms/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => config('variable.zenziva_userkey'),
            'passkey' => config('variable.zenziva_passkey'),
            'nohp' => $hp,
            'pesan' => $message,
        ));

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);

        curl_close($curlHandle);

        if ($err) {
            Log::error(__METHOD__, ['err' => $err]);
            return null;
        } else {
            $data = array(
                'users_id' => $users_id,
                'commands' => 'sms',
                'transaction_id' => $results->messageId,
                'hp' => $hp,
                'body' => $message,
                'type' => 'zenziva',
                'status' => 'sending',
            );
            $history = History::create($data);
            return $history->id;
        }
    }

    /**
     * @param $users_id
     * @param $hp
     * @param $message
     * @param int $timeout
     * @return mixed
     */
    public static function sendSMSZenzivaMasking($users_id, $hp, $message, $timeout = 60)
    {
        $hp = Utility::cleanNumber($hp); // menghilangkan simbol

        $body = [
            'userkey' => config('variable.zenziva_masking_userkey'),
            'passkey' => config('variable.zenziva_masking_passkey'),
            'nohp' => $hp,
            'pesan' => $message,
            'type' => 'otp',
            'res' => 'json',
        ];

        $body = http_build_query($body);
        $url = config('variable.zenziva_masking_url') . '/smsapi.php';
        $headers[] = 'content-type: application/x-www-form-urlencoded';
        $curlHandle = curl_init($url);

        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curlHandle, CURLOPT_POST, 1);

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);

        Log::debug(__METHOD__, ['curl' => $curl, 'err' => $err, 'result' => $results]);

        curl_close($curlHandle);

        if ($err) {
            Log::error(__METHOD__, ['err' => $err]);
            return null;
        } else {
            $data = array(
                'users_id' => $users_id,
                'commands' => 'sms',
                'transaction_id' => $results->messageId,
                'hp' => $hp,
                'body' => $message,
                'data' => json_encode($results),
                'type' => 'zenziva-masking',
                'status' => 'sending',
            );
            $history = History::create($data);
            return $history->id;
        }
    }
}
