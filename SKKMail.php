<?php

namespace App\SKKLibrary;

use App\History;
use App\Mail\BasicEmail;
use App\Notification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Swift_TransportException;

class SKKMail
{
    /**
     * @param $users_id
     * @param $toEmail
     * @param $toName
     * @param $fromEmail
     * @param $fromName
     * @param $subject
     * @param $body
     * @param null $cc
     * @param null $bcc
     * @param null $attachment
     * @param $referenceId
     */
    public static function send($users_id, $toEmail, $toName, $fromEmail, $fromName, $subject, $body, $cc = null, $bcc = null, $attachment = null, $referenceId)
    {
        $data = [
            'to_email' => $toEmail,
            'to_name' => $toName,
            'from_email' => ($fromEmail) ? $fromEmail : config('mail.from.address'),
            'from_name' => ($fromName) ? $fromName : config('mail.from.name'),
            'subject' => $subject,
            'body' => $body,
            'cc' => $cc,
            'bcc' => $bcc,
            'attachment' => $attachment,
            'html' => true,
        ];

        try {
            Mail::send(new BasicEmail($users_id, $data));
        } catch (Swift_TransportException $e) {
            Log::error(__METHOD__, ['error' => $e, 'data' => $data]);
        }

        Utility::addRecipient($toEmail, $toName, 'email');

        $history = new History();
        $history->users_id = $users_id;
        $history->commands = 'email';
        $history->reference_id = $referenceId;
        $history->hp = $fromEmail;
        $history->data = json_encode($data);
        $history->status = 'send';
        $history->message = 'sukses';
        $history->save();

        $notif = new Notification();
        $notif->users_id = $users_id;
        $notif->type = 'email';
        $notif->hp = $toEmail;
        $notif->title = $subject;
        $notif->body = $body;
        $notif->data = json_encode($data);
        $notif->attachment = $attachment;
        $notif->reference_id = $referenceId;
        $notif->history_id = $history->id;
        $notif->save();

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);
    }
}