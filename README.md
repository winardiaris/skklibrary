# SKKLibrary

Library for SKK Application based from laravel

## Required

- laravel/laravel
- doctrine/dbal
- predis/predis
- maatwebsite/excel

## Install In Laravel

```bash
cd to/your/laravel/project
git submodule add https://gitlab.com/winardiaris/skklibrary.git app/SKKLibrary
git submodule init
git submodule update
git submodule foreach git pull origin master

composer install
composer require doctrine/dbal
composer require predis/predis
composer require maatwebsite/excel
```
