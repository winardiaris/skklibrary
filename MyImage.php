<?php

namespace App\SKKLibrary;


use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class MyImage
{
    /**
     * @param      $filename
     * @param      $path
     * @param      $width
     * @param      $height
     *
     * @return null|string
     * @internal param bool $newfile
     */
    public static function createThumbnail($filename, $path, $width, $height)
    {
        $hasFolder = Utility::contains('/', $filename);

        if ($hasFolder) {
            $x = explode("/", $filename);
            $name = end($x);
        } else {
            $name = $filename;
        }

        $filename = config('app.name') . '/thumbnail/' . $width . 'x' . $height . '/' . $name;

        return MyImage::resizeAspectRatio($filename, $path, $width, $height);
    }

    /**
     * @param      $filename
     * @param      $path
     * @param      $width
     * @param      $height
     * @param bool $newfile
     *
     * @return null
     */
    public static function resizeAspectRatio($filename, $path, $width, $height, $newfile = false)
    {
        $manager = new ImageManager(array('driver' => 'gd'));
        $image = $manager->make($path)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });
        $image->save($path);

        if ($newfile) {
            $tmp = explode(".", $filename);
            $filename = $tmp[0] . '_' . $width . 'x' . $height . '.' . $tmp[1];
        }

        $url = Utility::uploadFile($filename, $path);

        return $url;
    }

    /**
     * @param     $filename
     * @param     $path
     * @param int $resize
     *
     * @return string|null
     */
    public static function cropSquareAndUploadImage($filename, $path, $resize = 500)
    {
        $img = Image::make($path);
        $hasFolder = Utility::contains('/', $filename);

        if ($hasFolder) {
            $x = explode("/", $filename);
            $name = end($x);
        } else {
            $name = $filename;
        }

        $croppath = public_path($name);
        $height = $img->getHeight();
        $width = $img->getWidth();
        $size = ($width > $height) ? $height : $width;
        $potrait = ($width > $height) ? false : true;

        $img->crop($size, $size, (!$potrait) ? intval(($width - $height) / 2) : 0, ($potrait) ? intval(($height - $width) / 2) : 0);
        $img->resize($resize, $resize);
        $img->save($croppath);

        $upload = Utility::uploadFile($filename, $croppath);
        unlink($croppath);

        return $upload;
    }
}
