<?php

namespace App\SKKLibrary;

class Crypt
{
    /**
     * @param $string
     * @param null $api_username
     * @param null $api_password
     * @param string $action
     * @return bool|string
     */
    public static function crypt($string, $api_username = null, $api_password = null, $action = 'e')
    {
        $secret_key = ($api_username) ? md5($api_username) : 'key92@fSkk!4man133f@#33giLw';
        $secret_iv = ($api_password) ? md5($api_password) : 'IVk4588fLin9iLW';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action === 'e') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
            $output = base64_encode($output);
        } elseif ($action === 'd') {
            $string = base64_decode($string);
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    /**
     * @param $token
     * @return mixed
     */
    public static function decryptToken($token)
    {
        $decrypt = Crypt::crypt($token, null, null, 'd');
        return json_decode($decrypt);
    }

    /**
     * @param $qrcode
     * @param bool $wbt
     * @return array|null
     */
    public function decryptQrCode($qrcode, $wbt = false)
    {
        $decrypt = Crypt::crypt($qrcode, null, null, 'd');
        $split = explode(" ", $decrypt);

        if (count($split) != 4) {
            if ($wbt) {
                return null;
            } else {
                Handler::handleWrongQrCodePayment();
            }
        }

        Utility::checkLimitMinMaxTransaction($split[2]);

        $result = [
            'api_id' => $split[0],
            'merchant_id' => $split[1],
            'amount' => $split[2],
            'key' => $split[3],
        ];

        return $result;
    }

    /**
     * @param $string
     * @param string $glue
     * @param int $length
     * @return string
     */
    public static function createKey($string, $glue = "-", $length = 8)
    {
        return implode($glue, str_split(Crypt::crypt($string), $length));
    }
}