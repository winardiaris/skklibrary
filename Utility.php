<?php

namespace App\SKKLibrary;

use App\History;
use App\LaciMartDevices;
use App\Recipient;
use App\Settings;
use App\SKKLibrary\Imports\GeneralImport;
use App\TCounter;
use App\TJurnal;
use App\TKartuSim;
use App\TRegister;
use App\TSaktilink;
use App\User;
use Carbon\Carbon;
use DateTime;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\RFCValidation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Predis\Connection\ConnectionException;
use SimpleXMLElement;

//use Symfony\Component\Console\Input\Input;

/**
 * Class Utility
 *
 * @package App\SKKLibrary
 */
class Utility
{
    // kalau nambah function/method simpan sesuai urutan abjad
    const numberPrefix = [
        '0814' => 'indosat',
        '0815' => 'indosat',
        '0816' => 'indosat',
        '0855' => 'indosat',
        '0856' => 'indosat',
        '0857' => 'indosat',
        '0858' => 'indosat',
        '0817' => 'xl',
        '0818' => 'xl',
        '0819' => 'xl',
        '0859' => 'xl',
        '0878' => 'xl',
        '0877' => 'xl',
        '0838' => 'axis',
        '0837' => 'axis',
        '0831' => 'axis',
        '0832' => 'axis',
        '0811' => 'halo',
        '0812' => 'telkomsel',
        '0813' => 'telkomsel',
        '0852' => 'telkomsel',
        '0853' => 'telkomsel',
        '0821' => 'telkomsel',
        '0823' => 'telkomsel',
        '0822' => 'telkomsel',
        '0851' => 'telkomsel',
        '0881' => 'smartfren',
        '0882' => 'smartfren',
        '0883' => 'smartfren',
        '0884' => 'smartfren',
        '0885' => 'smartfren',
        '0886' => 'smartfren',
        '0887' => 'smartfren',
        '0888' => 'smartfren',
        '0896' => 'three',
        '0897' => 'three',
        '0898' => 'three',
        '0899' => 'three',
        '0895' => 'three',
        '999' => 'bolt',
        '998' => 'bolt',
    ];

    const mimeImage = [
        'image/gif',
        'image/jpg',
        'image/jpeg',
        'image/png',
        'image/wbmp',
        'image/xpm',
    ];

    const mimeVideo = [
        'video/avi',
        'video/msvideo',
        'video/x-msvideo',
        'video/mpeg',
        'video/mp4',
    ];

    /**
     * @param        $hp
     * @param null   $name
     * @param string $type
     */
    public static function addRecipient($hp, $name = null, $type = 'sms')
    {
        $recipient = Recipient::where('hp', $hp)->where('type', $type)->first();
        if ($recipient) {
            Recipient::find($recipient->id)->update(['hit' => intval($recipient->hit + 1), 'type' => $type]);
        } else {
            Recipient::create([
                'hp' => $hp,
                'name' => $name,
                'type' => $type,
                'hit' => 1,
            ]);
        }
    }

    /**
     * @param SimpleXMLElement $object
     * @param array            $data
     */
    public static function arrayToXml(SimpleXMLElement $object, array $data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $new_object = $object->addChild($key);
                Utility::arrayToXml($new_object, $value);
            } else {
                $object->addChild($key, $value);
            }
        }
    }

    /**
     * @param User $user
     * @param      $bodyData
     *
     * @return array|bool
     */
    public static function callbackToMerchant(User $user, $bodyData)
    {
        ksort($bodyData);

        $url = $user->callback;
        $headers[] = 'content-type: application/json';
        $json = json_encode($bodyData);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $info = curl_getinfo($ch);
        $data = curl_exec($ch);
        $data = json_decode($data, true);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $debugData['http_code'] = $httpcode;
        $debugData['url'] = $url;
        $debugData['method'] = 'POST';
        $debugData['headers'] = $headers;
        $debugData['request'] = $bodyData;
        $debugData['response'] = $data;
        $debugData['info'] = $info;

        Log::debug(__METHOD__, ['data' => $data]);

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = "callback-merchant";
        $history->data = json_encode($debugData);

        if (!curl_errno($ch)) {
            curl_close($ch);

            $history->status = 'sukses';
            $history->save();

            $result = Handler::result($httpcode, null, $debugData);

            Log::debug(__METHOD__, $result);

            return $result;
        } else {

            $history->status = 'error';
            $history->save();

            Log::error(__METHOD__, ['url' => $url, 'data' => $debugData]);

            return false;
        }
    }

    /**
     * @param Request $request
     */
    public static function checkConnection(Request $request)
    {
        Utility::simpleHistory('test-connection', 'check-connection', 'sukses', null, $request->ip());
        Handler::handleSuccess(__METHOD__, ['connection' => 'connected']);
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } elseif ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    /**
     * @param      $users_id
     * @param      $command
     * @param      $status
     * @param null $message
     * @param null $data
     * @param bool $duplicate
     *
     * @return History
     */
    public static function simpleHistory($users_id, $command, $status, $message = null, $data = null, $duplicate = true)
    {
        if ($duplicate) {
            $history = new History;
            $history->users_id = $users_id;
            $history->commands = $command;
            $history->status = $status;
            $history->message = $message;

            if ($data != null) {
                $history->data = json_encode($data);
            }

            $history->save();
        } else {
            $get = History::where('commands', $command)->where('users_id', $users_id);
            $history = $get->first();

            if ($history) {
                $get->update([
                    'status' => $status,
                    'message' => $message,
                    'data' => json_encode($data),
                ]);

                $history = $get->first();
            } else {
                $history = new History;
                $history->users_id = $users_id;
                $history->commands = $command;
                $history->status = $status;

                if ($data != null) {
                    $history->data = json_encode($data);
                }

                $history->save();
            }
        }

        Log::debug(__METHOD__, json_decode($history, true));

        return $history;
    }

    /**
     *
     */
    public static function checkIsMaintenance()
    {
        if (Utility::getSetting('maintenance_mode')) {
            Handler::handleMaintenance();
        }
    }

    /**
     * @param $key
     *
     * @return bool|\Illuminate\Config\Repository|mixed
     */
    public static function getSetting($key)
    {
        $tag = (new Settings())->getTable();
        $a = false;
        try {
            $exists = Redis::smembers($tag . ":all");
            if ($exists) {
                $a = true;
            }
        } catch (ConnectionException $e) {
            Log::error($e->getMessage());
        }


        if ($a) {
            $setting = null;
            $exists = Redis::smembers($tag . ":all");
            foreach ($exists as $id) {
                $fromRedis = json_decode(Redis::get($tag . ':' . $id));
                if ($fromRedis->key === $key) {
                    $setting = $fromRedis;
                }
            }
        } else {
            $setting = Settings::where('key', $key)->first();
        }


        if ($setting) {
            if ($setting->type === 'boolean') {
                return ($setting->value > 0) ? true : false;
            } elseif ($setting->type === 'int') {
                return intval($setting->value);
            } elseif ($setting->type === 'enum') {
                return explode(',', $setting->value);
            } else {
                return $setting->value;
            }
        } else {
            return null;
        }
    }

    /**
     * @param $noAgt
     * @param $amount
     */
    public static function checkLimitTransaction($noAgt, $amount)
    {
        Utility::checkLimitMinMaxTransaction($amount);
        if (Utility::getSetting('check_limit_transaction')) {
            $limit = Utility::getSetting('limit_transaction');
            $sum = TKartuSim::where('cdate', now())->where('no_agt', $noAgt)->sum('nett');
            $total = intval($sum + $amount);
            if ($total > $limit) {
                Handler::handleLimitTransaction($sum, $limit);
            }
        }
    }

    /**
     * @param $amount
     */
    public static function checkLimitMinMaxTransaction($amount)
    {
        $min = Utility::getSetting('limit_min_transaction');
        $max = Utility::getSetting('limit_max_transaction');

        if ($min['enable']) {
            if ($amount < $min['value']) {
                Handler::handleLimitMinimalTransaction($min['value']);
            }
        }

        if ($max['enable']) {
            if ($amount > $max['value']) {
                Handler::handleLimitMaximalTransaction($max['value']);
            }
        }
    }

    /**
     * @param User $user
     * @param      $hp
     * @param      $target
     * @param bool $handlerError
     *
     * @return bool
     */
    public static function checkRecipientExist(User $user, $hp, $target, $handlerError = false)
    {
        $valid = false;
        $exist = false;
        $recipient = null;

        // get recipient
        if ($target === 'mycoop') {
            $recipient = TRegister::select('id_register')->where('nomor_ponsel', $hp)->orWhere('email', $hp)->first();
            if ($recipient) {
                $exist = true;
                $valid = true;
            }
        }

        if ($target === Utility::getDefaultTarget()) {
            $recipient = TSaktilink::select('id_saktilink', 'url')->where('hp', $hp)->orWhere('email', $hp)->first();
            if ($recipient) {
                $exist = true;
            }
        }

        if ($target === 'lacimart') {
            $recipient = LaciMartDevices::select('laci_mart_id', 'device_id', 'email')->where('email', $hp)->first();
            if ($recipient) {
                $exist = true;
            }
        }

        // administrator
        if ($user != null && SKKAuth::isAdministrator($user)) {
            if ($exist) {
                $valid = true;
            }

            if (Utility::isEmail($hp)) {
                $valid = true;
            }

            if ($target === 'any') {
                $valid = true;
            }
        }

        if ($user != null && $user->role === 'koperasi' && $target === Utility::getDefaultTarget() && $exist) {
            $url = $user->koperasi->url_anggota;
            $valid = ($url === $recipient->url);
        }

        // lacimart
        if ($user != null && $user->role === 'lacimart' && $exist) {
            $valid = $user->lacimart->enable && ($user->lacimart->id == $recipient->laci_mart_id);
        }


        if (!$valid && $handlerError) {
            ($target === 'any') ? Handler::handleWrongTarget() : Handler::handleNotFoundRecipient($target);
        }

        return $valid;
    }

    /**
     * @param        $string
     * @param string $format
     *
     * @return bool
     */
    public static function isDateTimeString($string, $format = 'Y-m-d H:i:s')
    {
        return (DateTime::createFromFormat($format, $string) !== false);
    }

    /**
     * @param      $email
     * @param bool $handlerError
     *
     * @return bool
     */
    public static function isEmail($email, $handlerError = false)
    {
        $emailIsValid = false;
        if (!empty($email)) {
            $domain = ltrim(stristr($email, '@'), '@') . '.';
            $user = stristr($email, '@', true);

            if (!empty($user) &&
                !empty($domain) &&
                checkdnsrr($domain) &&
                Utility::isEmailValid($email)) {
                $emailIsValid = true;
            }
        }

        if ($handlerError && !$emailIsValid) {
            Handler::handleWrongEmail();
        }

        return $emailIsValid;
    }

    /**
     * @param $email
     *
     * @return bool
     */
    public static function isEmailValid($email)
    {
        $validator = new EmailValidator();
        $multipleValidations = new MultipleValidationWithAnd([
            new RFCValidation(),
            new DNSCheckValidation(),
        ]);

        return $validator->isValid($email, $multipleValidations);
    }

    /**
     * @return bool
     */
    public static function isLocal()
    {
        return (config('app.env') === 'local');
    }

    /**
     * @return bool
     */
    public static function isDevelopment()
    {
        return (config('app.env') === 'development');
    }

    /**
     * @return bool
     */
    public static function isProduction()
    {
        return (config('app.env') === 'production');
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public static function checkSignatureRequest(Request $request)
    {
        $header = $request->header('Authorization');
        $header = explode(" ", $header);
        $userAuth = $header[1];
        $user = Utility::getUserFromAuth($userAuth);

        $sign = $request->input('signature');
        $requestData = $request->all();

        unset($requestData['signature']);
        ksort($requestData);

        $json = json_encode($requestData);

        if (Crypt::crypt($json, $user->api_username, $user->api_password, 'e') === $sign) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $userAuth
     *
     * @return mixed|null
     */
    public static function getUserFromAuth($userAuth)
    {
        $auth = [];
        $users = User::all();
        foreach ($users as $user) {
            $username = $user->api_username;
            $password = $user->api_password;
            $localAuth = base64_encode("$username:$password");
            $auth[$localAuth] = $user;
        }

        if (!isset($auth[$userAuth])) {
            return null;
        } else {
            return $auth[$userAuth];
        }
    }

    /**
     * @param $string
     *
     * @return string
     */
    public static function cleanString($string)
    {
        return strtolower(str_replace(' ', '', $string));
    }

    /**
     * @param $needle
     * @param $haystack
     *
     * @return bool
     */
    public static function contains($needle, $haystack)
    {
        return strpos($haystack, $needle) !== false;
    }

    public static function createDateFilter(Request $request, $routeName = null)
    {
        $route = ($routeName) ? route($routeName) : $request->url();
        $from = (isset($request->from)) ? $request->input('from') : '';
        $to = (isset($request->to)) ? $request->input('to') : '';

        $filter = '<form class="form-horizontal" method="GET" action="' . $route . '">
            <div class="row">
                 <div class="col-md-4">
                    <input type="text" name="from" class="form-control datepicker" autocomplete="off"
                    placeholder="Dari" value="' . $from . '">
                 </div>
                 <div class="col-md-4">
                    <input type="text" name="to" class="form-control datepicker" autocomplete="off"
                    placeholder="Sampai" value="' . $to . '">
                 </div>
                  <div class="col-md-4">
                  <div <div class="btn-group" role="group">
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                    <button type="button" class="btn btn-danger" id="clear"><i class="fa fa-backspace"></i></button>
                  </div>
                  </div>
            </div>
            </form>';

        return $filter;
    }

    public static function createSearchFilter(Request $request, $routeName)
    {
        $route = ($routeName) ? route($routeName) : $request->url();
        $filter = ' <form class="form-horizontal" method="GET" action="' . $route . '">
                  <div class="input-group">';

        if (isset($request->search) and !is_array($request->search)) {
            $filter .= ' <input type="text" value="' . $request->input('search') . '" name="search" class="form-control"
                             placeholder="Cari...">';
        } else {
            $filter .= '  <input type="text" name="search" class="form-control" placeholder="Cari...">';
        }

        $filter .= '<div class="input-group-append">
                    <button class="btn btn-secondary" type="submit"><i class="fa fa-search"></i> </button>
                  </div>
                  </div>
                </form>';

        return $filter;
    }

    /**
     * @param Request $request
     * @param         $total
     * @param         $perPage
     * @param         $currentPage
     *
     * @return string
     */
    public static function createPagination(Request $request, $total, $perPage, $currentPage)
    {
        $ba = 3;
        $fl = 5;
        $lastPage = ceil($total / $perPage);
        $a = '<ul role="navigation" class="pagination">';

        if ($currentPage > 1) {
            // prev button
            $a .= ' <li class="page-item">
        <a href="' . Utility::buildUrl($request, intval($currentPage - 1)) . '" class="page-link"><</a>
      </li>';

            // first button
            if ($currentPage > ($ba + $fl)) {
                $a .= ' <li class="page-item"><a href="' . Utility::buildUrl($request, 1) . '" class="page-link">1</a></li>';
            }

            if ($currentPage - $ba > 1) {
                $a .= '<li aria-disabled="true" class="page-item disabled"><span class="page-link">...</span></li>';
            }

            // before current
            for ($i = 1; $i <= $ba; $i++) {
                $page = intval(($currentPage - 1) - ($ba - $i));
                if ($page >= 1) {
                    $a .= ' <li class="page-item">
                    <a href="' . Utility::buildUrl($request, $page) . '" class="page-link">' . $page . '</a>
                  </li>';
                }
            }
        }

        // current button
        $a .= '<li aria-current="page" class="page-item active"><span class="page-link">' . $currentPage . '</span></li>';

        if ($currentPage < $lastPage) {
            // after current
            if (($currentPage + $ba) < $lastPage) {
                for ($i = 1; $i <= $ba; $i++) {
                    $page = intval($currentPage + $i);
                    $a .= ' <li class="page-item">
                    <a href="' . Utility::buildUrl($request, $page) . '" class="page-link">' . $page . '</a>
                  </li>';
                }
            }

            if ($currentPage + $ba < $lastPage) {
                $a .= '<li aria-disabled="true" class="page-item disabled"><span class="page-link">...</span></li>';
            }

            // last button
            if ($currentPage + ($ba + $fl) < $lastPage) {
                $a .= ' <li class="page-item"><a href="' . Utility::buildUrl($request, $lastPage) . '" class="page-link">' . $lastPage . '</a></li>';
            }

            // next button
            $a .= ' <li class="page-item">
        <a href="' . Utility::buildUrl($request, intval($currentPage + 1)) . '" class="page-link">></a>
      </li>';
        }

        $a .= '</ul>';

        return $a;
    }

    /**
     * @param Request $request
     * @param         $page
     *
     * @return string
     */
    public static function buildUrl(Request $request, $page)
    {
        return $request->url() . '?' . http_build_query(array_merge($request->query(), ['page' => $page]));
    }

    /**
     * @return false|string
     */
    public static function generateExpired()
    {
        $plus = Utility::getSetting('trx_expired');

        return date("Y-m-d H:i:s", strtotime("+$plus day"));
    }

    /**
     * @return string
     */
    public static function generateIsoTime()
    {
        $date = Carbon::now(config('app.timezone'));
        date_default_timezone_set(config('app.timezone'));
        $fmt = $date->format('Y-m-d\TH:i:s');
        $ISO8601 = sprintf("$fmt.%s%s", substr(microtime(), 2, 3), date('P'));

        return $ISO8601;
    }

    /**
     * @param     $cuid
     * @param int $plus
     *
     * @return string
     */
    public static function generateNoSlip($cuid, $plus = 1)
    {
        $year = date('y');
        $month = date('m');
        $jurnal = TJurnal::select('noslip')
            ->where('cuid', $cuid)
            ->where('noslip', 'like', $year . '.%')
            ->orderBy('cdate', 'DESC')
            ->orderBy('noslip', 'DESC')
            ->first();

        $last = explode('.', $jurnal['noslip']);
        $noslip = $jurnal ? "$year.$month.$cuid." . (end($last) + $plus) : "$year.$month.$cuid." . +$plus;

        return $noslip;
    }

    public static function generatedRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * @param $cuid
     * @param $id
     *
     * @return string
     */
    public static function generateRefId($cuid, $id)
    {
        return $cuid . '-' . $id . time();
    }

    /**
     * @param User $user
     * @param      $hp
     * @param      $referenceId
     * @param bool $resend
     *
     * @return array
     */
    public static function generateSMSVerification(User $user, $hp, $referenceId, $resend = false)
    {
        $otp = Utility::addOrUpdateOTP($hp, $referenceId);
        $code = $otp->code;
        $message = [
            'Dari SAKTI.Cash: Verifikasi pembayaran untuk nomer referensi ' . $referenceId . ' Anda adalah ' . $code,
            'From SAKTI.Cash: Payment verification for your reference ' . $referenceId . ' number is ' . $code,
            'Kode otentikasi untuk pembayaran (' . $referenceId . ') via SAKTI.Cash adalah ' . $code,
        ];

        $result = [
            'id' => $otp->id,
            'code' => $otp->code,
            'message' => $message[rand(1, count($message) - 1)],
            'expired_time' => $otp->expired_time,
        ];

        if ($otp->expired_time > now() || $resend) {
            if (config('app.env') === 'development' || config('app.env') === 'production') {
                $history = History::select('id')
                    ->where('commands', 'sms')
                    ->where('hp', $hp)
                    ->where('reference_id', $referenceId)
                    ->where('code', $code)
                    ->first();

                if (!$history) {
                    SMS::send($user, $hp, $result['message'], 'mycoop', $referenceId);
                }
            }

            $history = History::select('id')
                ->where('users_id', $user->id)
                ->where('commands', 'create-verification')
                ->where('hp', $hp)
                ->where('reference_id', $referenceId)
                ->first();

            if (!$history) {
                $log = new History();
                $log->users_id = ($user->id) ? $user->id : 'system';
                $log->commands = 'create-verification';
                $log->reference_id = $referenceId;
                $log->transaction_datetime = $otp->expired_time;
                $log->hp = $hp;
                $log->code = $code;
                $log->save();
            }
        }

        return $result;
    }

    /**
     * @param $hp
     * @param $referenceId
     *
     * @return mixed
     */
    public static function addOrUpdateOTP($hp, $referenceId)
    {
        $otp = Otp::where('hp', $hp)->where('reference_id', $referenceId);
        $exist = $otp->first();
        $rand = rand(100000, 999999);
        $expiredTime = new DateTime();
        $expiredTime->modify("+" . Utility::getSetting('otp_expired') . " minutes");

        if ($exist) {
            $expired = ($exist->expired_time < now()) ? true : false;
            if ($expired) {
                $otp->update([
                    'hp' => $hp,
                    'code' => $rand,
                    'expired_time' => $expiredTime,
                ]);
            }

            return Otp::find($exist->id);
        } else {
            if ($hp) {
                return Otp::create([
                    'code' => $rand,
                    'hp' => $hp,
                    'reference_id' => $referenceId,
                    'expired_time' => $expiredTime,
                ]);
            }
        }
    }

    /**
     * @param int $plus
     *
     * @return int
     */
    public static function generateVoucher($plus = 1)
    {
        $exist = TCounter::find(1)->first();
        $count = ($exist) ? $exist->count : 0;

        return intval($count + $plus);
    }

    public static function getDateForSpecificDay($startDate, $endDate, $weekdayNumber)
    {
        $startDate = strtotime($startDate);
        $endDate = strtotime($endDate);

        $dateArr = [];

        do {
            if (date("w", $startDate) != $weekdayNumber) {
                $startDate += (24 * 3600); // add 1 day
            }
        } while (date("w", $startDate) != $weekdayNumber);

        while ($startDate <= $endDate) {
            $dateArr[] = date('Y-m-d', $startDate);
            $startDate += (7 * 24 * 3600); // add 7 days
        }

        return ($dateArr);
    }

    /**
     * @return float
     */
    public static function getCurrentMilis()
    {
        return round(microtime(true) * 1000);
    }

    /**
     * @param $url
     *
     * @return string
     */
    public static function getFileFromUrl($url)
    {
        $file = file_get_contents($url);
        $filename = basename($url);
        $localPath = storage_path('app') . '/' . $filename;
        file_put_contents($localPath, $file);

        return $localPath;
    }

    /**
     * @param $users_id
     * @param $select
     * @param $where
     *
     * @return mixed
     */
    public static function getHistory($users_id, $select, $where)
    {
        $history = History::select($select)->where('users_id', $users_id)->where($where)->get();
        Log::debug(__METHOD__, json_decode(json_encode($history), true));

        return $history;
    }

    /**
     * @param $transactionDate
     *
     * @return mixed
     */
    public static function getLastFeeKoperasi($transactionDate)
    {
        $commands = 'update-fee';
        $fee = Utility::getFee('all');
        $history = History::select('data')->where('kartusim_id', $fee->id)->where('commands', $commands)->where('created_at', ' < ', $transactionDate)->orderBy('created_at', 'desc')->first();
        if ($history) {
            $data = json_decode($history->data);
            $result = $data->new->fee_merchant;
        } else {
            $result = $fee->fee_merchant;
        }

        return $result;
    }

    /**
     * @param $code
     *
     * @return mixed|null
     */
    public static function getMessageResponse($code)
    {
        $message = [
            '00' => 'Transaction Success',
            '05' => 'Transaction failed / general error',
            '13' => 'INVALID AMOUNT',
            '78' => 'ALREADY PAID(Transaction Success)',
            '76' => 'Invalid To Account',
            '101' => 'Ilegal signature(inquiry Status)',
            '300' => 'No Transaction Found',
            '91' => 'Error Database',
        ];

        if (isset($message[$code])) {
            return $message[$code];
        } else {
            return null;
        }
    }

    /**
     * @param $url
     *
     * @return string
     */
    public static function getMimeFromUrl($url)
    {
        $result = "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $results = explode("\n", trim(curl_exec($ch)));
        foreach ($results as $line) {
            if (strtok($line, ':') == 'Content-Type') {
                $parts = explode(":", $line);
                $result = trim($parts[1]);
            }
        }

        return $result;
    }

    /**
     * @param        $hp
     * @param string $target
     *
     * @return string
     */
    public static function getRecipentNameTarget($hp, $target = 'mycoop')
    {
        $result = "";
        if ($target === 'mycoop') {
            $recipient = TRegister::select('nama')->where('nomor_ponsel', $hp)->orWhere('email', $hp)->first();
            if ($recipient) {
                $result = $recipient->nama;
            }
        }

        if ($target === Utility::getDefaultTarget()) {
            $recipient = TSaktilink::select('nama')->where('hp', $hp)->orWhere('email', $hp)->first();
            if ($recipient) {
                $result = $recipient->nama;
            }
        }

        return $result;
    }

    public static function getModelByTable($table)
    {
        foreach (get_declared_classes() as $class) {
            if (is_subclass_of($class, 'Illuminate\Database\Eloquent\Model')) {
                $model = new $class;
                if ($model->getTable() === $table) {
                    return $class;
                }

            }
        }

        return false;
    }

    public static function getDefaultTarget()
    {
        if (array_key_exists('fcm_api_default', config('variable'))) {
            $target = config('variable.fcm_api_default');
        } else {
            $target = 'saktilink';
        }

        return $target;
    }

    public static function getDefaultSaktilinkName()
    {
        $target = Utility::getDefaultTarget();
        if ($target === 'saktilink') {
            $name = config('label.saktilink');
        } else {
            $name = config('label')[$target];
        }

        return $name;
    }

    public static function getTargetFromUser(User $user)
    {
        $target = null;
        $role = $user->role;
        if ($role === 'middleware') {
            $target = 'mycoop';
        } elseif ($role === 'koperasi') {
            $target = Utility::getDefaultTarget();
        } elseif ($role === 'lacimart') {
            $target = 'lacimart';
        } else {
            $target = 'any';
        }

        return $target;
    }

    /**
     * @param       $filePath
     * @param Model $model
     * @param       $fieldRow
     *
     * @return bool
     */
    public static function importToModel($filePath, Model $model, $fieldRow)
    {
        // composer require maatwebsite/excel

        $allowedExtension = Utility::getSetting('allowed_file_import');
        $ext = File::extension($filePath);
        if (in_array($ext, $allowedExtension)) {
            $collection = new GeneralImport($model, $fieldRow);
            Excel::import($collection, $filePath);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param       $filePath
     * @param Model $model
     * @param       $fieldRow
     *
     * @return mixed|null
     */
    public static function importToArrayObject($filePath, Model $model, $fieldRow)
    {
        $tmp = [];
        $allowedExtension = Utility::getSetting('allowed_file_import');
        $ext = File::extension($filePath);

        if (in_array($ext, $allowedExtension)) {

            $collection = (new GeneralImport($model, $fieldRow))->toArray($filePath);
            $data = $collection[0];

            if (count($data[0]) == count($fieldRow)) {
                foreach ($data as $row) {
                    $new = [];
                    foreach ($fieldRow as $key => $value) {
                        $new[$key] = $row[$value];
                    }
                    array_push($tmp, $new);
                }

                return json_decode(json_encode($tmp));
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param      $hp
     * @param bool $handlerError
     *
     * @return bool
     */
    public static function isAllowedNumber($hp, $handlerError = false)
    {
        $allowed = false;
        $hp = Utility::cleanNumber($hp);
        $hp = preg_replace('/\D/', '', $hp);
        $front3 = substr($hp, 0, 3);
        $front4 = substr($hp, 0, 4);

        if ($front3 === '999' or $front3 === '998') {
            $allowed = true;
        } else {
            foreach (Utility::numberPrefix as $number => $operator) {
                if ($number == $front4) {
                    $allowed = true;
                }
            }
        }

        if ($handlerError && !$allowed) {
            Handler::handleWrongNumber();
        }

        return $allowed;
    }

    /**
     * @param $hp
     *
     * @return mixed
     */
    public static function cleanNumber($hp)
    {
        $hp = str_replace('+', '', $hp);
        $hp = str_replace('-', '', $hp);
        $hp = str_replace(' ', '', $hp);
        $front = substr($hp, 0, 2);

        if ($front === '62') {
            $hp = str_replace('62', '0', $hp);
        }

        return $hp;
    }

    /**
     * @param $users_id
     * @param $referenceId
     *
     * @return bool
     */
    public static function isReferenceIdExist($users_id, $referenceId)
    {
        $hasReference = History::select('id')->where('users_id', $users_id)
            ->where('reference_id', $referenceId)->first();

        if ($hasReference) {
            Handler::handleTransactionExist($referenceId);
        }

        return false;
    }

    /**
     * @param $string
     *
     * @return bool
     */
    public static function isJson($string)
    {
        json_decode($string);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @param       $array
     * @param array $value
     *
     * @return array|null
     */
    public static function removeArrayByValue($array, $value = [])
    {
        $array = array_flip($array);
        foreach ($value as $val) {
            unset($array[$val]);
        }
        $array = array_flip($array);

        return $array;
    }

    /**
     *
     */
    public static function stripXSS()
    {
        $sanitized = Utility::cleanArray(Input::get());
        Input::merge($sanitized);
    }

    public static function stripXSSv2(Request $request)
    {
        $sanitized = Utility::cleanArray($request->all());
        $request->merge($sanitized);
    }

    /**
     * @param $array
     *
     * @return array
     */
    public static function cleanArray($array)
    {
        $result = [];
        foreach ($array as $key => $value) {
            $key = strip_tags($key);
            if (is_array($value)) {
                $result[$key] = Utility::cleanArray($value);
            } else {
                $result[$key] = trim(strip_tags($value)); // Remove trim() if you want to.
            }
        }

        return $result;
    }

    /**
     * @param $object
     *
     * @return mixed
     */
    public static function toArray($object)
    {
        return json_decode(json_encode($object), true);
    }

    /**
     * @param        $value
     * @param int    $decimal
     * @param string $point
     *
     * @return string
     */
    public static function toDecimal($value, $decimal = 2, $point = ".")
    {
        return number_format((float) $value, $decimal, $point, '');
    }

    /**
     * @param $object
     *
     * @return mixed
     */
    public static function toObject($object)
    {
        return json_decode(json_encode($object));
    }

    /**
     * @param Request $request
     * @param         $user
     * @param         $commands
     * @param         $old
     * @param         $new
     * @param null    $id
     * @param null    $value
     */
    public static function userLog(Request $request, $user, $commands, $old, $new, $id = null, $value = null)
    {
        $data = [
            'old' => $old,
            'new' => $new,
        ];

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = $commands;
        $history->data_id = $id;
        $history->body = $value;
        $history->hp = $request->ip();
        $history->data = json_encode($data);
        $history->save();
    }

    /**
     *
     */
    public static function updateSettingFile()
    {
        Artisan::call('db:seed', ['--class' => 'SettingFile']);
        Artisan::call('optimize');
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        Artisan::call('config:cache');
    }

    /**
     * @param $filename
     * @param $data
     *
     * @return string
     */
    public static function uploadText($filename, $data)
    {
        $localPath = storage_path('app') . "/$filename";
        $file = fopen($localPath, 'w') or die('Cannot open file:  ' . $localPath);

        if (is_array($data)) {
            fwrite($file, json_encode($data));
        } else {
            fwrite($file, $data);
        }

        fclose($file);

        return Utility::uploadFile($filename, $localPath);
    }

    public static function uploadFile($filename, $localPath)
    {
        $file = file_get_contents($localPath);
        $created = Storage::disk('minio')->put($filename, $file);

        if ($created) {
            $publicUrl = Storage::disk('minio')->url($filename);
            Storage::delete($filename);

            return $publicUrl;
        } else {
            return null;
        }
    }

    /**
     * @param Request $request
     * @param         $rules
     */
    public static function validator(Request $request, $rules)
    {
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            Handler::handleErrorValidation($validator->errors());
        }
        Log::debug(__METHOD__, ['ip' => $request->ip(), 'path' => $request->path(), 'request' => Utility::toArray($request->all())]);
    }

    /**
     * @param $xml
     *
     * @return array|mixed
     */
    public static function xmlToArray($xml)
    {
        libxml_use_internal_errors(true);
        $xml = simplexml_load_string($xml);
        $array = json_decode(json_encode((array) $xml), true);
        $array = [$xml->getName() => $array];

        return $array;
    }

    /**
     * @param $type
     *
     * @return array
     */
    public static function xmlTagHelper($type)
    {
        $res = [];
        $res['type'] = str_replace('req', 'res', $type);
        if ($type === 'reqnotification') {
            $res['tag'] = 'notification';
        } else {
            $res['tag'] = 'data';
        }

        return $res;
    }
}
