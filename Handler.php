<?php

namespace App\SKKLibrary;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use SimpleXMLElement;

class Handler
{
    public static function handleBlockedAccount()
    {
        $res = Handler::handleError(__METHOD__, 'Blocked Account');
        Handler::returnJson($res, 201);
    }

    /**
     * @param      $tag
     * @param      $message
     * @param null $data
     * @param int  $status
     *
     * @return array
     */
    public static function handleError($tag, $message, $data = null, $status = 201)
    {
        Handler::setHeader($status);
        $result = Handler::result($status, $message, $data);

        if ($message === 'debug') {
            Log::debug($tag, $result);
        } else {
            Log::error($tag, $result);
        }

        return $result;
    }

    /**
     * @param $errors
     */
    public static function handleErrorValidation($errors)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.validation'), $errors, 400);
        Handler::returnJson($res, 400);
    }

    /**
     * @param bool $web
     *
     * @return $this|bool
     */
    public static function handleExpiredOtp($web = false)
    {
        $message = config('message.id.error.otp_expired');
        if ($web) {
            return redirect()->back()->with('error', $message);
        } else {
            $res = Handler::handleError(__METHOD__, $message);
            Handler::returnJson($res);
        }

        return false;
    }

    /**
     *
     */
    public static function handleExpiredTransaction()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.expired_transaction'));
        Handler::returnJson($res);
    }

    /**
     * @param $error
     *
     * @return array
     */
    public static function handleFailedAccouting($error)
    {
        return Handler::handleError(__METHOD__, 'Failed', ['message' => 'Gagal', 'tipe' => 'Pembayaran', 'err' => $error]);
    }

    /**
     * @param null $data
     */
    public static function handleFailedSendEmail($data = null)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.send_email'), $data);
        Handler::returnJson($res);
    }

    /**
     * @param Request $request
     */
    public static function handleForbidden(Request $request = null)
    {
        Utility::simpleHistory('auth', '403', 'forbidden', null, ($request) ? $request->all() : null);
        $res = Handler::handleError(__METHOD__, config('message.id.403'), null, 403);
        Handler::returnJson($res, 403);
    }

    /**
     *
     */
    public static function handleIsNotMitra()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_merchant'), null, 401);
        Handler::returnJson($res, 401);
    }

    /**
     * @param $max
     */
    public static function handleLimitMaximalTransaction($max)
    {
        $res = Handler::handleError(__METHOD__, 'jumlah transfer tidak diizinkan  di atas Rp' . $max);
        Handler::returnJson($res);
    }

    /**
     * @param $min
     */
    public static function handleLimitMinimalTransaction($min)
    {
        $res = Handler::handleError(__METHOD__, 'jumlah transfer tidak diizinkan  di bawah Rp' . $min);
        Handler::returnJson($res);
    }

    /**
     * @param $limit
     */
    public static function handleLimitSaldo($limit)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.limit_balance') . ' Rp.' . $limit);
        Handler::returnJson($res);
    }

    /**
     * @param $total
     * @param $limit
     */
    public static function handleLimitTransaction($total, $limit)
    {
        $res = Handler::handleError(__METHOD__, 'sudah mencapai limit transaksi per hari Rp' . number_format($limit, 0, ',', '.') . '. Total hari ini: Rp' . number_format($total, 0, ',', '.'));
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleMaintenance()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.maintenance_mode'), null, 503);
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleNotEnoughBalance()
    {
        $res = Handler::handleError(__METHOD__, 'Saldo Anda tidak cukup');
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleNotFoundMerchantTransaction()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_merchant_transaction'));
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleNotFoundMerchant()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_merchant'));
        Handler::returnJson($res);
    }

    /**
     * @param $type
     */
    public static function handleNotFoundRecipient($type)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient') . ' di ' . strtoupper($type));
        Handler::returnJson($res);
    }

    /**
     * @param $code
     */
    public static function handleNotFoundTransaction($code)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_transaction') . ' (' . $code . ')');
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleSelfTransaction()
    {
        $res = Handler::handleError(__METHOD__, 'Tidak bisa membayar QR Code pembayaran milik sendiri');
        Handler::returnJson($res);
    }

    /**
     * @param $data
     */
    public static function handleServerNotConnected($data)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.server_not_connected'), $data);
        Handler::returnJson($res);
    }

    /**
     * @param      $tag
     * @param null $data
     */
    public static function handleSuccess($tag, $data = null)
    {
        Handler::setHeader(200);
        $result = Handler::result(200, 'success', $data);
        Log::debug($tag, $result);
        Handler::returnJson($result, 200);
    }

    /**
     * @param $id
     */
    public static function handleTransactionExist($id)
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.duplicate_transaction') . ' (' . $id . ')');
        Handler::returnJson($res);
    }

    /**
     * @param $transaction_id
     */
    public static function handleTransactionUnpaid($transaction_id)
    {
        $response = [
            'status' => 'unpaid',
            'transaction_id' => $transaction_id,
        ];

        $res = Handler::handleError(__METHOD__, config('message.id.transaction_unpaid'), $response);
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleUnderZeroTransaction()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.under_zero_transaction'));
        Handler::returnJson($res);
    }

    /**
     * @param Request $request
     */
    public static function handleUserNotExist(Request $request)
    {
        Utility::simpleHistory('auth', '401', 'Unauthorized', null, $request->all());
        $res = Handler::handleError(__METHOD__, config('message.id.401'), null, 401);
        Handler::returnJson($res, 401);
    }

    /**
     * @param Request|null $request
     */
    public static function handleWrongEmail(Request $request = null)
    {
        Utility::simpleHistory('email', 'send', 'gagal', null, ($request) ? $request->all() : null);
        $res = Handler::handleError(__METHOD__, config('message.id.error.email'));
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleWrongNumber()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.number'));
        Handler::returnJson($res);
    }

    /**
     * @param bool $web
     *
     * @return $this|bool
     */
    public static function handleWrongOtp($web = false)
    {
        $message = config('message.id.error.otp');
        if ($web) {
            return redirect()->back()->with('error', $message);
        } else {
            $res = Handler::handleError(__METHOD__, $message);
            Handler::returnJson($res);
        }

        return false;
    }

    /**
     *
     */
    public static function handleWrongPin()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.pin'));
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleWrongTarget()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.target'));
        Handler::returnJson($res);
    }

    /**
     *
     */
    public static function handleWrongQrCodePayment()
    {
        $res = Handler::handleError(__METHOD__, config('message.id.error.not_found_qrcode'));
        Handler::returnJson($res);
    }

    /**
     * @param      $response
     * @param      $message
     * @param null $data
     *
     * @return array
     */
    public static function result($response, $message, $data = null)
    {
        if ($data === null) {
            return array(
                'status' => array(
                    'response' => $response,
                    'message' => $message,
                ),
            );
        } else {
            return array(
                'status' => array(
                    'response' => $response,
                    'message' => $message,
                ),
                'data' => $data,
            );
        }
    }

    /**
     * @param      $data
     * @param null $httpCode
     */
    public static function returnJson($data, $httpCode = null)
    {
        $code = ($httpCode != null) ? $httpCode : 201;
        Handler::setHeader($code);
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }

    /**
     * @param        $data
     * @param string $xmltag
     * @param int    $status
     */
    public static function returnXml($data, $xmltag = 'data', $status = 200)
    {
        Handler::setHeader($status);
        header("Content-Type:application/xml");

        $xml = new SimpleXMLElement("<$xmltag/>");
        Utility::arrayToXml($xml, $data);
        $xml = $xml->asXML();
        echo $xml;
        die();
    }

    /**
     * @param $status
     */
    public static function setHeader($status)
    {
        switch ($status) {
            case 200:
                header("HTTP/1.1 200 OK");
                break;
            case 201:
                header("HTTP/1.1 201 Created");
                break;
            case 400:
                header("HTTP/1.1 400 Bad Request");
                break;
            case 401:
                header("HTTP/1.1 401 Unauthorized");
                break;
            case 403:
                header("HTTP/1.1 403 Forbidden");
                break;
            case 503:
                header("HTTP/1.1 503 Service Unavailable");
                break;
            default:
                header("HTTP/1.1 500 Internal Server Error");
                break;
        }
    }

}