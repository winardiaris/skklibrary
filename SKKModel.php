<?php

namespace App\SKKLibrary;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Cache;
use ReflectionMethod;

/**
 * @property int $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class SKKModel extends Model
{
    public $builder;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->builder = $this->getConnection()->getSchemaBuilder();
    }

    public function getColumns($excepts = [])
    {
        $columns = $this->builder->getColumnListing($this->getTable());

        if (!empty($excepts)) {
            $result = array();
            foreach ($columns as $column) {
                if (!in_array($column, $excepts)) {
                    array_push($result, $column);
                }
            }
            return $result;
        } else {
            return $columns;
        }
    }

    public function getType($columnName)
    {
        return $this->builder->getColumnType($this->getTable(), $columnName);
    }

    public function getColumnAndType()
    {
        $columns = $this->builder->getColumnListing($this->getTable());
        $result = array();
        foreach ($columns as $column) {
            $result[$column] = $this->getType($column);
        }
        return $result;
    }

    public function hasColumn($columnName)
    {
        return $this->builder->hasColumn($this->getTable(), $columnName);
    }

    public function getDistinct($columnName)
    {
        return $this->select($columnName)->distinct()->get();
    }

    public function getList($columnName)
    {
        $result = array();
        foreach ($this->getDistinct($columnName)->toArray() as $item) {
            array_push($result, $item[$columnName]);
        }
        return $result;
    }

    public function getAllRelations(Model $model = null, $heritage = 'all')
    {
        // from https://stackoverflow.com/questions/20334843/get-all-relationships-from-eloquent-model
        $model = $model ?: $this;
        $modelName = get_class($model);
        $types = ['children' => 'Has', 'parents' => 'Belongs', 'all' => ''];
        $heritage = in_array($heritage, array_keys($types)) ? $heritage : 'all';
        if (Cache::has($modelName . "_{$heritage}_relations")) {
            return Cache::get($modelName . "_{$heritage}_relations");
        }

        $reflectionClass = new \ReflectionClass($model);
        $traits = $reflectionClass->getTraits();    // Use this to omit trait methods
        $traitMethodNames = [];
        foreach ($traits as $name => $trait) {
            $traitMethods = $trait->getMethods();
            foreach ($traitMethods as $traitMethod) {
                $traitMethodNames[] = $traitMethod->getName();
            }
        }

        // Checking the return value actually requires executing the method.  So use this to avoid infinite recursion.
        $currentMethod = collect(explode('::', __METHOD__))->last();
        $filter = $types[$heritage];
        $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);  // The method must be public
        $methods = collect($methods)->filter(function ($method) use ($modelName, $traitMethodNames, $currentMethod) {
            $methodName = $method->getName();
            if (!in_array($methodName, $traitMethodNames)   //The method must not originate in a trait
                && strpos($methodName, '__') !== 0  //It must not be a magic method
                && $method->class === $modelName    //It must be in the self scope and not inherited
                && !$method->isStatic() //It must be in the this scope and not static
                && $methodName != $currentMethod    //It must not be an override of this one
            ) {
                $parameters = (new ReflectionMethod($modelName, $methodName))->getParameters();
                return collect($parameters)->filter(function ($parameter) {
                    return !$parameter->isOptional();   // The method must have no required parameters
                })->isEmpty();  // If required parameters exist, this will be false and omit this method
            }
            return false;
        })->mapWithKeys(function ($method) use ($model, $filter) {
            $methodName = $method->getName();
            $relation = $model->$methodName();  //Must return a Relation child. This is why we only want to do this once
            if (is_subclass_of($relation, Relation::class)) {
                $type = (new \ReflectionClass($relation))->getShortName();  //If relation is of the desired heritage
                if (!$filter || strpos($type, $filter) === 0) {
                    return [$methodName => get_class($relation->getRelated())]; // ['relationName'=>'relatedModelClass']
                }
            }
            return false;   // Remove elements reflecting methods that do not have the desired return type
        })->toArray();

        Cache::forever($modelName . "_{$heritage}_relations", $methods);
        return $methods;
    }
}
