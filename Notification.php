<?php

namespace App\SKKLibrary;

class Notification
{
    public static function curlNotif($path, $body = null, $post = false)
    {
        $body = ($body) ? json_encode($body) : '';
        $curl = curl_init();
        $url = env('NF_URL', config('variable.nf_url')) . '/api' . $path;
        $auth = Notification::setAuth(env('NF_USERNAME', config('variable.nf_username')), env('NF_PASSWORD', config('variable.nf_password')));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => ($post) ? "POST" : "GET",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Authorization: Basic " . $auth,
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Content-Type: application/json",
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return json_decode($response);
        }
    }

    public static function getTelegramChatId($target = 'mycoop')
    {
        $path = '/' . $target . '/get/telegram';

        return Notification::curlNotif($path);
    }

    public static function getFcm($hp, $target = 'mycoop')
    {
        $path = '/' . $target . '/get/fcm';
        $body = [
            'hp' => $hp,
        ];

        return Notification::curlNotif($path, $body, true);
    }

    public static function getKoperasi()
    {
        $path = '/get/koperasi';

        return Notification::curlNotif($path);
    }

    public static function sendFCM($hp, $title, $message, $image = null, $action = null, $data = null, $target = 'mycoop')
    {
        $path = '/' . $target . '/send/fcm';
        $body = [
            'hp' => $hp,
            'title' => $title,
            'message' => $message,
            'image' => $image,
            'action' => $action,
            'data' => $data,
        ];

        return Notification::curlNotif($path, $body, true);
    }

    public static function sendSMS($hp, $message, $target = 'mycoop')
    {
        $path = '/' . $target . '/send/sms';
        $body = [
            'hp' => $hp,
            'message' => $message,
        ];

        return Notification::curlNotif($path, $body, true);
    }

    public static function sendTelegram($chatId, $message, $target = 'mycoop')
    {
        $path = '/' . $target . '/send/telegram';
        $body = [
            'chat_id' => $chatId,
            'message' => $message,
        ];

        return Notification::curlNotif($path, $body, true);
    }

    public static function setAuth($username, $password)
    {
        return base64_encode("$username:$password");
    }

}
