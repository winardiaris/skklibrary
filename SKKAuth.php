<?php

namespace App\SKKLibrary;

use App\User;

class SKKAuth
{
    /**
     * @param User $user
     * @param bool $handlerError
     *
     * @return bool
     */
    public static function isAdministrator(User $user, $handlerError = false)
    {
        $valid = false;
        if ($user->role === 'administrator' or $user->role === 'support') {
            $valid = true;
        }

        if (!$valid && $handlerError) {
            Handler::handleForbidden();
        }

        return $valid;
    }

}
