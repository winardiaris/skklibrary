<?php

namespace App\SKKLibrary;

use App\History;
use App\Notification;
use App\Recipient;
use CURLFile;
use Illuminate\Support\Facades\Log;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Exception;
use TelegramBot\Api\Types\InputMedia\ArrayOfInputMedia;
use TelegramBot\Api\Types\InputMedia\InputMediaPhoto;
use TelegramBot\Api\Types\InputMedia\InputMediaVideo;

class Telegram
{
    // kalau nambah function/method simpan sesuai urutan abjad

    /**
     * @param        $users_id
     * @param string $chatId
     * @param        $message
     * @param null   $media
     * @param null   $document
     * @param        $referenceId
     */
    public static function send($users_id, $chatId = '-314176584', $message, $media = null, $document = null, $referenceId)
    {
        Utility::addRecipient($chatId, null, 'telegram');
        $txt = null;
        $med = null;
        $doc = null;

        try {
            $bot = new BotApi(config('variable.telegram_token'));
            Telegram::sendText($bot, $chatId, $message);
            $txt = 'sukses';

        } catch (\TelegramBot\Api\Exception $e) {
            $txt = $e->getMessage();
        }

        if ($media) {
            try {
                $bot = new BotApi(config('variable.telegram_token'));
                Telegram::sendMedia($bot, $chatId, $media);
                $med = 'sukses';

            } catch (\TelegramBot\Api\Exception $e) {
                $med = $e->getMessage();
            }
        }

        if ($document) {
            try {
                $bot = new BotApi(config('variable.telegram_token'));
                Telegram::sendDocument($bot, $chatId, $document);
                $doc = 'sukses';

            } catch (\TelegramBot\Api\Exception $e) {
                $doc = $e->getMessage();
            }
        }

        $data = [
            'text' => $message,
            'text_send' => $txt,
            'media' => $media,
            'media_send' => $med,
            'document' => $document,
            'document_send' => $doc,
        ];

        $history = new History();
        $history->users_id = $users_id;
        $history->commands = 'telegram';
        $history->reference_id = $referenceId;
        $history->hp = $chatId;
        $history->data = json_encode($data);
        $history->status = 'send';
        $history->message = $txt;
        $history->save();

        $notif = new Notification();
        $notif->users_id = $users_id;
        $notif->type = 'telegram';
        $notif->hp = $chatId;
        $notif->body = $message;
        $notif->attachment = json_encode(['media' => $media, 'document' => $document]);
        $notif->reference_id = $referenceId;
        $notif->history_id = $history->id;
        $notif->save();

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);
    }

    /**
     * @param BotApi $bot
     * @param        $chatId
     * @param        $message
     *
     * @return \TelegramBot\Api\Types\Message
     */
    public static function sendText(BotApi $bot, $chatId, $message)
    {
        return $bot->sendMessage($chatId, trim($message), 'markdown');
    }

    /**
     * @param BotApi $bot
     * @param        $chatId
     * @param        $mediaUrl
     */
    public static function sendMedia(BotApi $bot, $chatId, $mediaUrl)
    {
        $inputMedia = ($mediaUrl != null) ? explode(',', str_replace(' ', '', $mediaUrl)) : null;

        if ($inputMedia && count($inputMedia) > 0) {
            $media = new ArrayOfInputMedia();
            foreach ($inputMedia as $item) {
                if (in_array(Utility::getMimeFromUrl($item), Utility::mimeImage)) {
                    $media->addItem(new InputMediaPhoto($item));
                }

                if (in_array(Utility::getMimeFromUrl($item), Utility::mimeVideo)) {
                    $media->addItem(new InputMediaVideo($item));
                }
            }

            if (count(json_decode($media->toJson(), true)) > 0) {
                $bot->sendMediaGroup($chatId, $media);
            }
        }

    }

    /**
     * @param BotApi $bot
     * @param        $chatId
     * @param        $document
     */
    public static function sendDocument(BotApi $bot, $chatId, $document)
    {
        $inputDocument = ($document != null) ? explode(',', str_replace(' ', '', $document)) : null;

        if ($inputDocument && count($inputDocument) > 0) {
            foreach ($inputDocument as $item) {
                try {
                    $localFile = Utility::getFileFromUrl($item);
                    $localDocument = new CURLFile($localFile);
                    $bot->sendDocument($chatId, $localDocument);
                } catch (Exception $e) {
                    $bot->sendMessage($chatId, trim($item));
                    $bot->sendMessage($chatId, trim('Gagal kirim dokumen, Error : ' . $e->getMessage()));
                }

            }
        }
    }

    public static function sync()
    {
        $bot = config('variable.telegram_token');
        $curlHandle = curl_init();

        curl_setopt_array($curlHandle, [
            CURLOPT_URL => "https://api.telegram.org/bot$bot/getUpdates",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Accept: */*",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Host: api.telegram.org",
                "cache-control: no-cache",
            ],
        ]);

        $curl = curl_exec($curlHandle);
        $err = curl_error($curlHandle);
        $results = json_decode($curl);
        Log::debug(__METHOD__, Utility::toArray($results));

        curl_close($curlHandle);

        if ($err) {
            Handler::handleError(__METHOD__, $err);
        }

        $create = [];
        $update = [];
        foreach ($results->result as $result) {
            $chatId = null;
            $name = null;

            if (isset($result->message)) {
                $chatId = $result->message->chat->id;
                $type = $result->message->chat->type;

                if ($type === 'supergroup' or $type === 'group') {
                    $name = $result->message->chat->title;
                } elseif ($type === 'private') {
                    if (isset($result->message->chat->last_name)) {
                        $name = $result->message->chat->first_name . ' ' . $result->message->chat->last_name;
                    } else {
                        $name = $result->message->chat->first_name;
                    }
                } else {
                    $name = "";
                }

            }

            if (isset($result->channel_post)) {
                $chatId = $result->channel_post->chat->id;
                $name = $result->channel_post->chat->title;
            }

            if ($chatId) {
                $recipient = Recipient::where('hp', $chatId)->where('type', 'telegram')->first();
                if ($recipient) {
                    Recipient::find($recipient->id)->update([
                        'name' => $name,
                    ]);
                    array_push($update, ['chat_id' => $chatId, 'name' => $name]);
                } else {
                    Recipient::create([
                        'hp' => $chatId,
                        'name' => $name,
                        'type' => 'telegram',
                        'hit' => 0,
                    ]);
                    array_push($create, ['chat_id' => $chatId, 'name' => $name]);
                }
            }
        }

        $result = [
            'create' => $create,
            'update' => $update,
        ];

        return $result;
    }
}
