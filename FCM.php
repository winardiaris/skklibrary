<?php

namespace App\SKKLibrary;

use App\History;
use App\LaciMart;
use App\LaciMartDevices;
use App\Notification;
use App\TDaftarKoperasi;
use App\TRegister;
use App\TSaktilink;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class FCM
{
    public static function getFcm($hp, $target, $limit, $users_id = null)
    {
        $result = array();
        if ($users_id != null) {
            $admin = User::select('id')->where('role', 'administrator')->orWhere('role', 'support')->orWhere('id', $users_id)->get();
        } else {
            $admin = User::select('id')->where('role', 'administrator')->orWhere('role', 'support')->get();
        }

        $notifications = Notification::select('users_id', 'reference_id', 'hp', 'title', 'body', 'created_at', 'image', 'action', 'data')
            ->where('type', 'fcm-' . $target)
            ->whereIn('users_id', $admin)
            ->where(function ($query) use ($hp) {
                $query->where('hp', 'all')
                    ->orWhere('hp', $hp)
                    ->orWhere('hp', 'like', '%' . $hp . '%');
            })
            ->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();

        foreach ($notifications as $notification) {
            $notification->data = json_decode($notification->data);

            if (Utility::contains(',', $notification->hp)) {
                $notification->hp = $hp;
                $notification->type = 'multiple';
            } elseif ($notification->hp === 'all') {
                $notification->type = 'all';
            } else {
                $notification->type = 'single';
            }

            array_push($result, $notification);
        }

        return $result;
    }

    /**
     * @param User   $user
     * @param string $target
     * @param        $hp
     * @param        $title
     * @param        $message
     * @param null   $image
     * @param        $action
     * @param        $data
     * @param        $referenceId
     *
     * @return array
     */
    public static function sendFcm(User $user, $target = 'mycoop', $hp, $title, $message, $image = null, $action, $data = null, $referenceId)
    {
        ini_set('memory_limit', '-1');
        if ($target === 'mycoop') {
            return FCM::sendFcmMyCoop($user, $hp, $title, $message, $image, $action, $data, $referenceId);
        } elseif ($target === 'saktilink') {
            return FCM::sendFcmSaktiLink($user, $hp, $title, $message, $image, $action, $data, $referenceId);
        } elseif ($target === 'wangpas') {
            return FCM::sendFcmWangpas($user, $hp, $title, $message, $image, $action, $data, $referenceId);
        } elseif ($target === 'lacimart') {
            return FCM::sendFcmLaciMart($user, $hp, $title, $message, $image, $action, $data, $referenceId);
        } else {
            Handler::handleWrongTarget();

            return null;
        }
    }

    /**
     * @param User $user
     * @param      $hp
     * @param      $title
     * @param      $message
     * @param null $image
     * @param      $action
     * @param null $data
     * @param      $referenceId
     *
     * @return array
     */
    public static function sendFcmMyCoop(User $user, $hp, $title, $message, $image = null, $action, $data = null, $referenceId)
    {
        $arrayToken = array();
        $recipients = array();
        $perPage = 900;
        $target = 'mycoop';
        $result = array();

        $notif = new Notification();
        $notif->users_id = $user->id;
        $notif->type = 'fcm-' . $target;
        $notif->action = $action;
        $notif->reference_id = $referenceId;
        $notif->hp = $hp;
        $notif->title = $title;
        $notif->body = $message;
        $notif->image = $image;
        $notif->data = is_array($data) ? json_encode($data) : $data;
        $notif->status = 'sending';
        $notif->save();

        $length = strlen($hp);
        if (strtolower($hp) === 'all') { // all devices
            $dataAnggota = TRegister::select('nomor_ponsel', 'nama', 'token_fcm')->where('token_fcm', '!=', 'null')->where('token_fcm', '!=', '')->get();
            foreach ($dataAnggota as $item) {
                array_push($arrayToken, $item->token_fcm);
                array_push($recipients, [
                    'hp' => $item->nomor_ponsel,
                    'nama' => $item->nama,
                    'type' => 'fcm-' . $target,
                ]);
            }
        } elseif ($length > 3) {
            if (Utility::contains(',', $hp)) { // multiple device separated by comma (,)
                $hp = str_replace(" ", "", $hp);
                $hp = explode(",", $hp);

                $dataAnggota = TRegister::select('nomor_ponsel', 'nama', 'token_fcm')
                    ->whereIn('nomor_ponsel', $hp)
                    ->where('token_fcm', '!=', 'null')
                    ->where('token_fcm', '!=', '')
                    ->get();
                foreach ($dataAnggota as $item) {
                    array_push($arrayToken, $item->token_fcm);
                    array_push($recipients, [
                        'hp' => $item->nomor_ponsel,
                        'nama' => $item->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                }
            } elseif (Utility::isAllowedNumber($hp) && $length >= 10 && $length <= 13 and !Utility::contains(',', $hp)) { // single device
                $anggota = TRegister::select('nomor_ponsel', 'nama', 'token_fcm')->where('nomor_ponsel', $hp)->first();
                if ($anggota) {
                    array_push($arrayToken, $anggota->token_fcm);
                    array_push($recipients, [
                        'hp' => $anggota->nomor_ponsel,
                        'nama' => $anggota->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                } else {
                    $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
                }
            } else {
                $result = Handler::handleError(__METHOD__, config('message.id.error.number'));
            }
        } else {
            $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
        }

        // save recipient to redis
        if (count($recipients) > 0) {
            Redis::sadd('notification:recipients', $referenceId);
            foreach ($recipients as $item) {
                Redis::sadd('notification:recipients:' . $referenceId, json_encode($item));
            }
        }

        // send fcm
        $arrayToken = array_unique($arrayToken);
        if (count($arrayToken) > 0) {
            if (count($arrayToken) >= $perPage) {
                $loop = ceil(count($arrayToken) / $perPage);

                for ($i = 1; $i <= $loop; $i++) {
                    $token = array_slice($arrayToken, intval($perPage * ($i - 1)), $perPage);
                    $a = FCM::send($notif->id, $token, $title, $message, $image, $action, $data, $target);
                    array_push($result, $a);
                }
            } else {
                $result = FCM::send($notif->id, $arrayToken, $title, $message, $image, $action, $data, $target);
            }
        }

        $dataResult = ['count' => count($arrayToken), 'result' => $result];
        $dataResult['reference_id'] = $referenceId;

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = 'fcm';
        $history->reference_id = $referenceId;
        $history->hp = is_array($hp) ? implode(',', $hp) : $hp;
        $history->data = json_encode($result);
        $history->status = 'send';
        $history->message = ($result) ? 'sukses' : 'gagal';
        $history->type = $target;
        $history->data = json_encode($dataResult);
        $history->save();

        Notification::find($notif->id)->update([
            'history_id' => $history->id,
            'status' => $history->status,
            'message' => $history->message,
        ]);


        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);

        return json_decode(json_encode($dataResult));
    }

    /**
     * @param        $idTable
     * @param        $arrayToken
     * @param        $title
     * @param        $body
     * @param null   $image
     * @param string $action
     * @param        $data
     * @param string $target
     *
     * @return mixed
     */
    public static function send($idTable, $arrayToken, $title, $body, $image = null, $action = '', $data = null, $target = 'mycoop')
    {
        $color = [
            'mycoop' => '#89216B',
            'saktilink' => '#F03A52',
            'wangpas' => '#155624',
            'lacimart' => '#2A9C79',
        ];

        $api = [
            'mycoop' => config('variable.fcm_api_mycoop'),
            'saktilink' => config('variable.fcm_api_saktilink'),
            'wangpas' => config('variable.fcm_api_wangpas'),
            'lacimart' => config('variable.fcm_api_lacimart'),
        ];

        $notifValue = array(
            'body' => $body,
            'title' => $title,
            'icon' => 'fcm_icon',
            'color' => $color[$target],
            'image' => $image,
            'vibrate' => 1,
            'sound' => 1,
            'click_action' => "FCM_PLUGIN_ACTIVITY",
        );

        if ($image) {
            $dataValue = array(
                'id' => $idTable,
                'body' => $body,
                'title' => $title,
                'action' => $action,
                'image' => $image,
                'picture' => $image,
                'data' => (is_array($data)) ? $data : trim($data),
            );
        } else {
            $dataValue = array(
                'id' => $idTable,
                'body' => $body,
                'title' => $title,
                'action' => $action,
                'data' => (is_array($data)) ? $data : trim($data),
            );
        }

        $ids = Utility::removeArrayByValue($arrayToken, ['null', '']);
        $fields = array(
            'registration_ids' => array_values($ids),
            'data' => $dataValue,
            'notification' => $notifValue,
        );

        Log::debug(__METHOD__ . '-before-send', $fields);

        $headers = array(
            'Authorization: key=' . $api[$target],
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('variable.fcm_url'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $response = curl_exec($ch);
        curl_close($ch);

        $debug = [
            'header' => $headers,
            'url' => config('variable.fcm_url'),
            'request' => $fields,
            'response' => $response,
        ];

        $result = Utility::isJson($response) ? json_decode($response) : $response;
        $ext = Utility::isJson($response) ? '.json' : '.txt';

        Log::debug(__METHOD__ . '-after-send', $debug);

        $name = (Utility::isJson($response) and isset($result->multicast_id)) ? $result->multicast_id : 'fcm-' . time();
        $filename = $name . $ext; //local
        $localPath = storage_path('app') . "/$filename";
        $file = fopen($localPath, 'w') or die('Cannot open file:  ' . $localPath);

        fwrite($file, $response);
        fclose($file);

        $filename = config('app.name') . '/fcm/fcm-' . $filename;
        $url = Utility::uploadFile($filename, $localPath);

        unset($result->results);
        $result->url = $url;

        Log::debug(__METHOD__, ['result' => Utility::toArray($result)]);

        return $result;
    }

    /**
     * @param User $user
     * @param      $hp
     * @param      $title
     * @param      $message
     * @param null $image
     * @param      $action
     * @param null $data
     * @param      $referenceId
     *
     * @return array
     */
    public static function sendFcmSaktiLink(User $user, $hp, $title, $message, $image = null, $action, $data = null, $referenceId)
    {
        $arrayToken = array();
        $recipients = array();
        $perPage = 900;
        $target = 'saktilink';
        $result = array();

        $notif = new Notification();
        $notif->users_id = $user->id;
        $notif->type = 'fcm-' . $target;
        $notif->action = $action;
        $notif->reference_id = $referenceId;
        $notif->hp = $hp;
        $notif->title = $title;
        $notif->body = $message;
        $notif->image = $image;
        $notif->data = is_array($data) ? json_encode($data) : $data;
        $notif->status = 'sending';
        $notif->save();

        $length = strlen($hp);
        if (strtolower($hp) === 'all') { // all devices
            if (SKKAuth::isAdministrator($user)) {
                $dataAnggota = TSaktilink::select('tokenfcm', 'hp', 'nama')->where('tokenfcm', '!=', 'null')->where('tokenfcm', '!=', '')->get();
            } else {
                $koperasi = TDaftarKoperasi::where('kd_koperasi', $user->kd_koperasi)->first();
                $dataAnggota = $koperasi->saktilink;
            }

            foreach ($dataAnggota as $item) {
                array_push($arrayToken, $item->tokenfcm);
                array_push($recipients, [
                    'hp' => $item->hp,
                    'nama' => $item->nama,
                    'type' => 'fcm-' . $target,
                ]);
            }
        } elseif ($length > 3) {
            if (Utility::contains(',', $hp)) { // multiple device separated by comma (,)
                $hp = str_replace(" ", "", $hp);
                $hp = explode(",", $hp);

                if (SKKAuth::isAdministrator($user)) {
                    $dataAnggota = TSaktilink::select('tokenfcm', 'hp', 'nama')
                        ->whereIn('hp', $hp)
                        ->where('tokenfcm', '!=', 'null')
                        ->where('tokenfcm', '!=', '')
                        ->get();
                } else {
                    $koperasi = TDaftarKoperasi::where('kd_koperasi', $user->kd_koperasi)->first();
                    $dataAnggota = $koperasi->saktilink->whereIn('hp', $hp);
                }

                foreach ($dataAnggota as $item) {
                    array_push($arrayToken, $item->tokenfcm);
                    array_push($recipients, [
                        'hp' => $item->hp,
                        'nama' => $item->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                }
            } elseif (Utility::isAllowedNumber($hp) && $length >= 10 && $length <= 13) { // single device
                $anggota = TSaktilink::select('hp', 'nama', 'tokenfcm')
                    ->where('hp', $hp)
                    ->where('tokenfcm', '!=', 'null')
                    ->where('tokenfcm', '!=', '')
                    ->first();
                if ($anggota) {
                    array_push($arrayToken, $anggota->tokenfcm);
                    array_push($recipients, [
                        'hp' => $anggota->hp,
                        'nama' => $anggota->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                } else {
                    $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
                }
            } else {
                $result = Handler::handleError(__METHOD__, config('message.id.error.number'));
            }
        } else {
            $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
        }

        // save recipient to redis
        if (count($recipients) > 0) {
            Redis::sadd('notification:recipients', $referenceId);
            foreach ($recipients as $item) {
                Redis::sadd('notification:recipients:' . $referenceId, json_encode($item));
            }
        }

        // send fcm
        $arrayToken = array_unique($arrayToken);
        if (count($arrayToken) > 0) {
            if (count($arrayToken) >= $perPage) {
                $loop = ceil(count($arrayToken) / $perPage);

                for ($i = 1; $i <= $loop; $i++) {
                    $token = array_slice($arrayToken, intval($perPage * ($i - 1)), $perPage);
                    $a = FCM::send($notif->id, $token, $title, $message, $image, $action, $data, $target);
                    array_push($result, $a);
                }
            } else {
                $result = FCM::send($notif->id, $arrayToken, $title, $message, $image, $action, $data, $target);
            }
        }

        $dataResult = ['count' => count($arrayToken), 'result' => $result];
        $dataResult['reference_id'] = $referenceId;

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = 'fcm';
        $history->reference_id = $referenceId;
        $history->hp = is_array($hp) ? implode(',', $hp) : $hp;
        $history->data = json_encode($result);
        $history->status = 'send';
        $history->message = 'sukses';
        $history->type = $target;
        $history->data = json_encode($dataResult);
        $history->save();

        Notification::find($notif->id)->update([
            'history_id' => $history->id,
            'status' => $history->status,
            'message' => $history->message,
        ]);

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);

        return json_decode(json_encode($dataResult));
    }

    public static function sendFcmWangpas(User $user, $hp, $title, $message, $image = null, $action, $data = null, $referenceId)
    {
        $arrayToken = array();
        $recipients = array();
        $perPage = 900;
        $target = 'wangpas';
        $result = array();

        $notif = new Notification();
        $notif->users_id = $user->id;
        $notif->type = 'fcm-' . $target;
        $notif->action = $action;
        $notif->reference_id = $referenceId;
        $notif->hp = $hp;
        $notif->title = $title;
        $notif->body = $message;
        $notif->image = $image;
        $notif->data = is_array($data) ? json_encode($data) : $data;
        $notif->status = 'sending';
        $notif->save();

        $length = strlen($hp);
        if (strtolower($hp) === 'all') { // all devices
            if (SKKAuth::isAdministrator($user)) {
                $dataAnggota = TSaktilink::select('tokenfcm', 'hp', 'nama')->where('tokenfcm', '!=', 'null')->where('tokenfcm', '!=', '')->get();
            } else {
                $koperasi = TDaftarKoperasi::where('kd_koperasi', $user->kd_koperasi)->first();
                $dataAnggota = $koperasi->saktilink;
            }

            foreach ($dataAnggota as $item) {
                array_push($arrayToken, $item->tokenfcm);
                array_push($recipients, [
                    'hp' => $item->hp,
                    'nama' => $item->nama,
                    'type' => 'fcm-' . $target,
                ]);
            }
        } elseif ($length > 3) {
            if (Utility::contains(',', $hp)) { // multiple device separated by comma (,)
                $hp = str_replace(" ", "", $hp);
                $hp = explode(",", $hp);

                if (SKKAuth::isAdministrator($user)) {
                    $dataAnggota = TSaktilink::select('tokenfcm', 'hp', 'nama')
                        ->whereIn('hp', $hp)
                        ->where('tokenfcm', '!=', 'null')
                        ->where('tokenfcm', '!=', '')
                        ->get();
                } else {
                    $koperasi = TDaftarKoperasi::where('kd_koperasi', $user->kd_koperasi)->first();
                    $dataAnggota = $koperasi->saktilink->whereIn('hp', $hp);
                }

                foreach ($dataAnggota as $item) {
                    array_push($arrayToken, $item->tokenfcm);
                    array_push($recipients, [
                        'hp' => $item->hp,
                        'nama' => $item->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                }
            } elseif (Utility::isAllowedNumber($hp) && $length >= 10 && $length <= 13) { // single device
                $anggota = TSaktilink::select('hp', 'nama', 'tokenfcm')->where('hp', $hp)->first();
                if ($anggota) {
                    array_push($arrayToken, $anggota->tokenfcm);
                    array_push($recipients, [
                        'hp' => $anggota->hp,
                        'nama' => $anggota->nama,
                        'type' => 'fcm-' . $target,
                    ]);
                } else {
                    $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
                }
            } else {
                $result = Handler::handleError(__METHOD__, config('message.id.error.number'));
            }
        } else {
            $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
        }

        // save recipient to redis
        if (count($recipients) > 0) {
            Redis::sadd('notification:recipients', $referenceId);
            foreach ($recipients as $item) {
                Redis::sadd('notification:recipients:' . $referenceId, json_encode($item));
            }
        }

        // send fcm
        $arrayToken = array_unique($arrayToken);
        if (count($arrayToken) > 0) {
            if (count($arrayToken) >= $perPage) {
                $loop = ceil(count($arrayToken) / $perPage);

                for ($i = 1; $i <= $loop; $i++) {
                    $token = array_slice($arrayToken, intval($perPage * ($i - 1)), $perPage);
                    $a = FCM::send($notif->id, $token, $title, $message, $image, $action, $data, $target);
                    array_push($result, $a);
                }
            } else {
                $result = FCM::send($notif->id, $arrayToken, $title, $message, $image, $action, $data, $target);
            }
        }

        $dataResult = ['count' => count($arrayToken), 'result' => $result];
        $dataResult['reference_id'] = $referenceId;

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = 'fcm';
        $history->reference_id = $referenceId;
        $history->hp = is_array($hp) ? implode(',', $hp) : $hp;
        $history->data = json_encode($result);
        $history->status = 'send';
        $history->message = 'sukses';
        $history->type = $target;
        $history->data = json_encode($dataResult);
        $history->save();

        Notification::find($notif->id)->update([
            'history_id' => $history->id,
            'status' => $history->status,
            'message' => $history->message,
        ]);

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);

        return json_decode(json_encode($dataResult));
    }

    public static function sendFcmLaciMart(User $user, $email, $title, $message, $image = null, $action, $data = null, $referenceId)
    {
        $arrayToken = array();
        $recipients = array();
        $perPage = 900;
        $target = 'lacimart';
        $result = array();


        $notif = new Notification();
        $notif->users_id = $user->id;
        $notif->type = 'fcm-' . $target;
        $notif->action = $action;
        $notif->reference_id = $referenceId;
        $notif->hp = $email;
        $notif->title = $title;
        $notif->body = $message;
        $notif->image = $image;
        $notif->data = is_array($data) ? json_encode($data) : $data;
        $notif->status = 'sending';
        $notif->save();

        $length = strlen($email);
        if (strtolower($email) === 'all') { // all devices
            if (SKKAuth::isAdministrator($user)) {
                $devices = LaciMartDevices::where('fcm_token', '!=', 'null')
                    ->where('fcm_token', '!=', null)
                    ->where('fcm_token', '!=', '')
                    ->get();
            } else {
                $laciMart = LaciMart::where('id', $user->kd_koperasi)->first();
                $devices = $laciMart->devices;
            }

            foreach ($devices as $item) {
                array_push($arrayToken, $item->fcm_token);
                array_push($recipients, [
                    'hp' => $item->email,
                    'nama' => $item->device_id,
                    'type' => 'fcm-' . $target,
                ]);
            }
        } elseif ($length > 3) {
            if (Utility::contains(',', $email)) { // multiple user separated by comma (,)
                $email = str_replace(" ", "", $email);
                $email = explode(",", $email);

                if (SKKAuth::isAdministrator($user)) {
                    $devices = LaciMartDevices::where('fcm_token', '!=', 'null')
                        ->where('fcm_token', '!=', null)
                        ->where('fcm_token', '!=', '')
                        ->whereIn('email', $email)
                        ->get();
                } else {
                    $laciMart = LaciMart::where('id', $user->kd_koperasi)->first();
                    $devices = $laciMart->devices->whereIn('email', $email);
                }

                foreach ($devices as $item) {
                    array_push($arrayToken, $item->fcm_token);
                    array_push($recipients, [
                        'hp' => $item->email,
                        'nama' => $item->device_id,
                        'type' => 'fcm-' . $target,
                    ]);
                }
            } else { // single user multiple devices
                $devices = LaciMartDevices::where('email', $email)
                    ->where('fcm_token', '!=', 'null')
                    ->where('fcm_token', '!=', null)
                    ->where('fcm_token', '!=', '')
                    ->get();
                if (count($devices) > 0) {
                    foreach ($devices as $item) {
                        array_push($arrayToken, $item->fcm_token);
                        array_push($recipients, [
                            'hp' => $item->email,
                            'nama' => $item->device_id,
                            'type' => 'fcm-' . $target,
                        ]);
                    }
                }
            }
        } else {
            $result = Handler::handleError(__METHOD__, config('message.id.error.not_found_recipient'));
        }

        // save recipient to redis
        if (count($recipients) > 0) {
            Redis::sadd('notification:recipients', $referenceId);
            foreach ($recipients as $item) {
                Redis::sadd('notification:recipients:' . $referenceId, json_encode($item));
            }
        }

        // send fcm
        $arrayToken = array_unique($arrayToken);
        if (count($arrayToken) > 0) {
            if (count($arrayToken) >= $perPage) {
                $loop = ceil(count($arrayToken) / $perPage);

                for ($i = 1; $i <= $loop; $i++) {
                    $token = array_slice($arrayToken, intval($perPage * ($i - 1)), $perPage);
                    $a = FCM::send($notif->id, $token, $title, $message, $image, $action, $data, $target);
                    array_push($result, $a);
                }
            } else {
                $result = FCM::send($notif->id, $arrayToken, $title, $message, $image, $action, $data, $target);
            }
        }

        $dataResult = ['count' => count($arrayToken), 'result' => $result];
        $dataResult['reference_id'] = $referenceId;

        $history = new History();
        $history->users_id = $user->id;
        $history->commands = 'fcm';
        $history->reference_id = $referenceId;
        $history->hp = is_array($email) ? implode(',', $email) : $email;
        $history->data = json_encode($result);
        $history->status = 'send';
        $history->message = 'sukses';
        $history->type = $target;
        $history->data = json_encode($dataResult);
        $history->save();

        Notification::find($notif->id)->update([
            'history_id' => $history->id,
            'status' => $history->status,
            'message' => $history->message,
        ]);

        // save to redis
        SKKRedis::getData(new Notification(), [$notif->id]);
        SKKRedis::getData(new History(), [$history->id]);

        return json_decode(json_encode($dataResult));
    }
}
