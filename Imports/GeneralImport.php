<?php

namespace App\SKKLibrary\Imports;

use App\SKKLibrary\SKKRedis;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;

class GeneralImport implements ToCollection
{
    use Importable;
    private $model;
    private $fieldRow;

    public function __construct(Model $model, $fieldRow)
    {
        $this->model = $model;
        $this->fieldRow = $fieldRow;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            $new = array();
            $find = $this->model::select($this->model->getKeyName());
            foreach ($this->fieldRow as $key => $value) {
                $new[$key] = $row[$value];
                $find->where($key, $row[$value]);
            }

            if (!$find->first()) {
                $save = $this->model::create($new);
                SKKRedis::getData($this->model, [$save->id]);
            }
        }
    }
}
