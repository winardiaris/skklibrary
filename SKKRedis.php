<?php

namespace App\SKKLibrary;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;


class SKKRedis
{
    /**
     * @param Request $request
     * @param Model   $model
     * @param         $query
     * @param null    $from
     * @param null    $to
     * @param int     $limit
     * @param int     $offset
     * @param null    $order
     * @param         $routeName
     * @param array   $relations
     *
     * @return mixed
     */
    public static function getAllDataRedis(Request $request, $model, $query, $from = null, $to = null, $limit = -1, $offset = 0, $order = null, $routeName = null, $relations = [])
    {
        ini_set('memory_limit', '-1');
        $primaryKey = $model->getKeyName();
        $tag = $model->getTable();
        $resultData = [];
        $query = json_decode(json_encode($query));

        $users_id = (isset($query->users_id) and !is_array($query->users_id)) ? $query->users_id : null;
        $search = (isset($query->search)) ? $query->search : null;
        $commands = (isset($query->commands)) ? $query->commands : null;

        $data = SKKRedis::getDataForRedis($model, $query, $from, $to, $limit, $offset, $order, $relations);

        if (count($data->data) > 0) {
            foreach ($data->data as $item) {
                array_push($resultData, $item);
                Redis::set($tag . ':' . $item->{$primaryKey}, json_encode($item));

                if ($users_id != null) {
                    Redis::set('tag:' . $tag . ':user:' . $users_id, $item->{$primaryKey});
                    Redis::sadd($tag . ':user:' . $users_id, $item->{$primaryKey});
                } elseif ($search != null) {
                    Redis::set('tag:' . $tag . ':search:' . $search, $item->{$primaryKey});
                    Redis::sadd($tag . ':search:' . $search, $item->{$primaryKey});
                } elseif ($from != null) {
                    $date = ($to != null) ? $from . '_' . $to : $to;
                    Redis::set('tag:' . $tag . ':date:' . $date, $item->{$primaryKey});
                    Redis::sadd($tag . ':date:' . $date, $item->{$primaryKey});
                } elseif ($commands != null) {
                    Redis::set('tag:' . $tag . ':commands:' . $commands, $item->{$primaryKey});
                    Redis::sadd($tag . ':commands:' . $commands, $item->{$primaryKey});
                } else {
                    Redis::set('tag:' . $tag . ':all', $item->{$primaryKey});
                    Redis::sadd($tag . ':all', $item->{$primaryKey});
                }
            }
        }

        $result['limit'] = $limit;
        $result['offset'] = $offset;
        $result['total'] = $data->total;
        $result['per_page'] = $limit;
        $result['current_page'] = ($offset / $limit) + 1;
        $result['data'] = $resultData;
        $result['to'] = intval($result['current_page'] * $result['per_page']);
        $result['from'] = intval($result['to'] - $result['per_page'] + 1);
        $result['links'] = Utility::createPagination($request, $result['total'], $result['per_page'], $result['current_page']);
        $result['filter']['date'] = Utility::createDateFilter($request, $routeName);
        $result['filter']['search'] = Utility::createSearchFilter($request, $routeName);

        ksort($result);

        return json_decode(json_encode($result));
    }

    /**
     * @param       $model
     * @param       $query
     * @param null  $from
     * @param null  $to
     * @param int   $limit
     * @param int   $offset
     * @param null  $order
     * @param array $relations
     *
     * @return mixed
     */
    public static function getDataForRedis($model, $query, $from = null, $to = null, $limit = -1, $offset = 0, $order = null, $relations = [])
    {
        ini_set('memory_limit', '-1');
        $primaryKey = $model->getKeyName();
        $dataModel = $model::select($primaryKey);
        $search = (isset($query->search)) ? $query->search : null;

        if ($search != null) {
            // todo sort by click

            $columns = $model->getColumns([$primaryKey, 'created_at', 'updated_at']);
            $dataModel->where($columns[0], $search);
            $isDatetime = Utility::isDateTimeString($search);
            foreach ($columns as $column) {
                $type = $model->getType($column);
                if ($type === 'string' && !$isDatetime) {
                    $dataModel->orWhere($column, 'like', '%' . $search . '%');
                    $dataModel->orWhere($column, $search);
                } elseif ($type === 'integer' && is_int($search)) {
                    $dataModel->orWhere($column, $search);
                } elseif ($type === 'float' && (is_double($search) or is_int($search))) {
                    $dataModel->orWhere($column, $search);
                } elseif ($type === 'datetime' && $isDatetime) {
                    $dataModel->orWhere($column, $search);
                } elseif ($type === 'boolean' && (is_double($search) or is_int($search)) && (intval($search) <= 1)) {
                    $dataModel->orWhere($column, $search);
                }
            }
        }

        if (!empty($query)) {
            $query = json_decode(json_encode($query), true);

            foreach ($query as $column => $value) {
                if ($model->hasColumn($column)) {
                    if (is_array($value)) {
                        if (in_array(strtolower($value[0]), ['like', '=', '!=', 'not like'])) {
                            $dataModel->where($column, $value[0], $value[1]);
                        } elseif (in_array(strtolower($value[0]), ['or', 'orwhere',])) {
                            if (in_array(strtolower($value[1]), ['like', '=', '!=', 'not like'])) {
//                            $query = ['field' => ['or', 'like','value']];
                                $dataModel->orWhere($column, $value[1], $value[2]);
                            } else {
//                            $query = ['field' => ['or', 'value']];
                                $dataModel->orWhere($column, $value[1]);
                            }
                        } else {
                            $dataModel->whereIn($column, $value);
                        }
                    } else {
                        $dataModel->where($column, $value);
                    }
                }
            }
        }

        if ($from != null) {
            if ($to != null) {
                $dataModel->whereBetween('created_at', [$from . ' 00:00:00', $to . ' 23:59:59']);
            } else {
                $dataModel->whereBetween('created_at', [$from . ' 00:00:00', date('Y-m-d H:i:s')]);
            }
        }

        if ($order != null && is_array($order)) {
            if (isset($order[0])) { // ['key','asc']
                $dataModel->orderBy($order[0], $order[1]);
            } else { //  ['key' => 'asc', 'key' => 'desc']
                foreach ($order as $key => $sort) {
                    $dataModel->orderBy($key, $sort);
                }
            }
        } else {
            if ($model->hasColumn('created_at')) {
                $dataModel->orderBy('created_at', 'desc');
            } else {
                $dataModel->orderBy($primaryKey, 'asc');
            }
        }

        $result['total'] = $dataModel->count();

        if ($limit === -1) {
            $dataModel = $dataModel->get();
        } else {
            $dataModel = $dataModel->skip($offset)->take($limit)->get();
        }

        $ids = array();
        foreach ($dataModel as $item) {
            array_push($ids, $item->{$primaryKey});
        }

        $dataRedis = SKKRedis::getData($model, $ids, $relations, false);
        $result['data'] = $dataRedis;

        return json_decode(json_encode($result));
    }

    /**
     * @param Model $model
     * @param       $id
     * @param array $relations
     *
     * @internal param $data
     */
    public static function updateDataRedis(Model $model, int $id, $relations = [])
    {
        SKKRedis::getData($model, [$id], $relations, true, true);
    }

    /**
     * @param Model $model
     * @param array $ids
     * @param array $relations
     * @param bool  $first
     * @param bool  $force
     *
     * @return mixed
     */
    public static function getData($model, $ids = [], $relations = [], $first = true, $force = false)
    {
        $primaryKey = $model->getKeyName();
        $tag = $model->getTable();
        $result = array();
        $count = count($ids);

        if (empty($relations)) {
            $r = $model->getAllRelations();
            $relations = array();
            foreach ($r as $relation => $class) {
                array_push($relations, $relation);
            }
        }

        if (!$force) {
            foreach ($ids as $index => $id) {
                $fromRedis = Redis::get($tag . ':' . $id);
                if ($fromRedis) {
                    unset($ids[$index]);
                    array_push($result, json_decode($fromRedis));
                }
            }
        }

        if (!empty($ids)) {
            $data = $model::whereIn($primaryKey, $ids);
            if (!empty($relations) && $relations[0] != false) {
                $data->with($relations);
            }
            $data = $data->get();
            if (count($data) > 0) {
                foreach ($data as $tmp) {
                    Redis::set($tag . ':' . $tmp->{$primaryKey}, json_encode($tmp));
                    Redis::sadd($tag . ':all', $tmp->{$primaryKey});
                    if (isset($tmp->users_id)) {
                        Redis::sadd($tag . ':user:' . $tmp->users_id, $tmp->{$primaryKey});
                    }
                    array_push($result, json_decode(json_encode($tmp)));
                }
            } else {
                $result = null;
            }
        }

        if ($count > 1) {
            return $result;
        } else {
            if ($result != null) {
                if ($first) {
                    return $result[0];
                } else {
                    return $result;
                }
            } else {
                return $result;
            }
        }
    }

    /**
     * @param Model  $model
     * @param string $userid
     */
    public static function cleanDataRedis(Model $model, $userid = 'all')
    {
        $tag = $model->getTable();

        if ($userid === 'all') {
            $all = "$tag:all";
        } else {
            $all = "$tag:user:$userid";
        }

        $sMembers = Redis::smembers($all);

        foreach ($sMembers as $item) {
            Redis::srem($all, $item);
            Redis::del("$tag:$item");
        }

        Redis::del("tag:$all");
    }
}